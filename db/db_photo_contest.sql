use photo_contest;
create table contest_categories
(
    contest_category_id int auto_increment
        primary key,
    name text not null,
    constraint contest_category_name_uindex
        unique (name) using hash
);

create table contests
(
    contest_id int auto_increment
        primary key,
    title text not null,
    contest_category_id int not null,
    is_open tinyint(1) default 1 null,
    created datetime not null,
    phase_1 datetime not null,
    phase_2 datetime not null,
    end_date datetime null,
    is_scored tinyint(1) default 0 null,
    constraint contests_title_uindex
        unique (title) using hash,
    constraint contests_contest_categories_contest_category_id_fk
        foreign key (contest_category_id) references contest_categories (contest_category_id)
);

create table security
(
    security_id int auto_increment
        primary key,
    user_name varchar(20) not null,
    pass_hash text not null,
    salt double not null,
    constraint security_user_name_uindex
        unique (user_name)
);

create table personal_info
(
    personal_info_id int auto_increment
        primary key,
    first_name varchar(20) not null,
    last_name varchar(20) not null,
    user_name varchar(20) not null,
    constraint personal_info_user_name_uindex
        unique (user_name),
    constraint personal_info_security_user_name_fk
        foreign key (user_name) references security (user_name)
);

create table jury
(
    jury_id int auto_increment
        primary key,
    personal_info_id int not null,
    contest_id int not null,
    constraint jury_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint jury_personal_info_id_fk
        foreign key (personal_info_id) references personal_info (personal_info_id)
);

create table organisers
(
    organiser_id int auto_increment
        primary key,
    personal_info_id int not null,
    constraint organisers_personal_info_id_uindex
        unique (personal_info_id),
    constraint organisers_personal_info_id_fk
        foreign key (personal_info_id) references personal_info (personal_info_id)
);

create table users
(
    user_id int auto_increment
        primary key,
    personal_info_id int not null,
    `rank` int null,
    constraint users_personal_info_id_uindex
        unique (personal_info_id),
    constraint users_personal_info_id_fk
        foreign key (personal_info_id) references personal_info (personal_info_id)
);

create table contest_entries
(
    contest_entry_id int auto_increment
        primary key,
    contest_id int not null,
    user_id int not null,
    constraint contest_entry_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contest_entry_users_id_fk
        foreign key (user_id) references users (user_id)
);

create table contest_invitations
(
    contest_invitation_id int auto_increment
        primary key,
    contest_id int not null,
    user_id int not null,
    constraint contest_invitations_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contest_invitations_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table entries
(
    entry_id int auto_increment
        primary key,
    contest_entry_id int not null,
    title text not null,
    description text not null,
    photo_location text not null,
    constraint entry_contest_entry_id_fk
        foreign key (contest_entry_id) references contest_entries (contest_entry_id)
);

create table photos
(
    photo_Id int auto_increment
        primary key,
    entry_id int not null,
    photo mediumblob not null,
    constraint photo_entries_entry_id_fk
        foreign key (entry_id) references entries (entry_id)
);

create table reviews
(
    review_id int auto_increment
        primary key,
    contest_entry_id int not null,
    author int not null,
    text_review text not null,
    score int not null,
    constraint review_contest_entry_id_fk
        foreign key (contest_entry_id) references contest_entries (contest_entry_id),
    constraint review_jury_id_fk
        foreign key (author) references jury (jury_id)
);

