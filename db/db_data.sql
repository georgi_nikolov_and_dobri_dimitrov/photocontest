use photo_contest;

INSERT INTO photo_contest.security (security_id, user_name, pass_hash, salt)
VALUES (1, 's3d', '153557795', 0),
       (2, 'test', '-39090786', 0),
       (3, 'ivo', '-2040430655', 0),
       (4, 'superMe', '2040927508', 0),
       (5, 'novik', '1270097911', 0);

INSERT INTO photo_contest.personal_info (personal_info_id, first_name, last_name, user_name)
VALUES (1, 'Dobri', 'Dimitrov', 's3d'),
       (2, 'test', 'test', 'test'),
       (3, 'Ivan', 'Ivanov', 'ivo'),
       (4, 'Marko', 'Katarov', 'superMe'),
       (5, 'Novak', 'Jokovski', 'novik');

INSERT INTO photo_contest.users (user_id, personal_info_id, `rank`)
VALUES (1, 3, 1001),
       (2, 4, 51),
       (3, 5, 0);

INSERT INTO photo_contest.organisers (organiser_id, personal_info_id)
VALUES (1, 1),
       (2, 2);

INSERT INTO photo_contest.contest_categories (contest_category_id, name)
VALUES (1, 'Cats'),
       (2, 'Dogs'),
       (3, 'Pets'),
       (4, 'City'),
       (5, 'Nature'),
       (6, 'Country'),
       (7, 'Sports'),
       (8, 'Drones');

INSERT INTO photo_contest.contests (contest_id, title, contest_category_id, created, phase_1, phase_2, end_date)
VALUES (1, 'Winter Pets 2021', 3, '2021-03-27 13:05:13', '2021-03-31 13:05:27', '2021-04-03 13:05:36',
        '2021-04-10 13:05:53'),
       (2, 'Winter Sports 2021', 7, '2021-01-01 13:06:49', '2021-01-27 13:07:06', '2021-05-27 13:07:35',
        '2021-06-27 13:07:53'),
       (3, 'Winter Dogs 2021', 2, '2021-02-27 13:08:44', '2021-03-11 13:08:57', '2021-03-27 13:09:09',
        '2021-06-27 13:09:13'),
       (4, 'Winter Cats 2021', 1, '2021-01-27 13:09:44', '2021-02-27 13:09:50', '2021-03-10 13:09:57',
        '2021-03-27 13:10:13');

INSERT INTO photo_contest.contest_entries (contest_entry_id, contest_id, user_id)
VALUES (1, 1, 1),
       (2, 1, 2),
       (3, 1, 3),
       (4, 2, 1),
       (5, 2, 2),
       (6, 2, 3),
       (7, 3, 1),
       (8, 3, 2),
       (9, 3, 3),
       (10, 4, 1),
       (11, 4, 2),
       (12, 4, 3);

INSERT INTO photo_contest.entries (entry_id, contest_entry_id, title, description, photo_location)
VALUES (1, 1, 'My parrot Fly', 'My parrot Fly', 'asd'),
       (2, 2, '1st time in snow', '1st time in snow', 'asd'),
       (3, 3, 'Snow play', 'Snow play', 'asd'),
       (4, 4, 'Jump in', 'Jump In', 'asd'),
       (5, 5, 'Faster', 'Faster', 'asd'),
       (6, 6, 'Team play', 'Team play', 'asd'),
       (7, 7, 'Johny & Snow', 'Johny & Snow', 'asd'),
       (8, 8, 'Max Snow', 'Max Snow', 'asd'),
       (9, 9, 'Catch', 'Catch', 'asd'),
       (10, 10, 'Snyauw', 'Snyauw', 'asd'),
       (11, 11, 'Help', 'Help', 'asd'),
       (12, 12, 'Snow-nya', 'Snow-nya', 'asd');

INSERT INTO photo_contest.jury (jury_id, personal_info_id, contest_id)
VALUES (1, 1, 1),
       (2, 2, 1),
       (3, 1, 2),
       (4, 2, 2),
       (5, 1, 3),
       (6, 2, 3),
       (7, 1, 4),
       (8, 2, 4);

INSERT INTO photo_contest.reviews (review_id, contest_entry_id, author, text_review, score)
VALUES (1, 1, 1, 'nice', 6),
       (2, 2, 1, 'good', 5),
       (3, 3, 1, 'super', 9),
       (4, 4, 1, 'perfect', 10),
       (5, 5, 1, 'nice', 5),
       (6, 6, 1, 'good', 4),
       (7, 7, 1, 'really ?', 2),
       (8, 8, 1, 'no', 1),
       (9, 9, 1, 'no', 2),
       (10, 10, 1, 'good', 7),
       (11, 11, 1, 'very nice', 8),
       (12, 12, 1, 'good', 6),
       (13, 1, 2, 'good', 5),
       (14, 2, 2, 'super', 9),
       (15, 3, 2, 'perfect', 10),
       (16, 4, 2, 'nice', 5),
       (17, 5, 2, 'good', 4),
       (18, 6, 2, 'really ?', 2),
       (19, 7, 2, 'no', 1),
       (20, 8, 2, 'no', 2),
       (21, 9, 2, 'very nice', 7),
       (22, 10, 2, 'very nice', 8),
       (23, 11, 2, 'nice', 6),
       (24, 12, 2, 'ok', 5);

