package com.example.photoContest.services.utils;

import java.time.LocalDateTime;

public abstract class Constants {

    public static final int MOCK_USER_PERSONAL_INFO_ID = 2;
    public static final String MOCK_USER_FIRST_NAME = "Goran";
    public static final String WRONG = "Wrong";
    public static final String MOCK_USER_LAST_NAME = "Mechkarov";
    public static final String MOCK_USER_USERNAME = "Meco";
    public static final int MOCK_USER_ID = 5;
    public static final int MOCK_USER_RANK = 5000;

    public static final int MOCK_ORGANISER_PERSONAL_INFO_ID = 7;
    public static final String MOCK_ORGANISER_FIRST_NAME = "Jonatan";
    public static final String MOCK_ORGANISER_LAST_NAME = "Spasov";
    public static final String MOCK_ORGANISER_USERNAME = "Sparrow";
    public static final int MOCK_ORGANISER_ID = 9;

    public static final int MOCK_SECURITY_ID_ORGANISER = 8;
    public static final int MOCK_SECURITY_ID_USER = 4;
    public static final String MOCK_PASS_HASH = "815212366";
    public static final String MOCK_PASS = "testtest";
    public static final double MOCK_SALT = 0;

    public static final int MOCK_JURY_PERSON_ID = 6;
    public static final int MOCK_REVIEW_ID = 61;
    public static final int MOCK_REVIEW_SCORE = 8;
    public static final String MOCK_REVIEW_TEXT = "Very Funny";

    public static final int MOCK_CONTEST_CATEGORY_ID = 10;
    public static final String MOCK_CONTEST_CATEGORY_NAME = "Happy days";
    public static final int MOCK_CONTEST_ID = 123;
    public static final String MOCK_CONTEST_TITLE = "First comes first served";
    public static final boolean MOCK_CONTEST_IS_OPEN = true;
    public static final boolean MOCK_CONTEST_IS_SCORED = false;
    public static final LocalDateTime TODAY = LocalDateTime.now();
    public static final LocalDateTime YESTERDAY = LocalDateTime.now().minusDays(1);
    public static final LocalDateTime TOMORROW = LocalDateTime.now().plusDays(1);

    public static final int MOCK_CONTEST_ENTRY_ID = 99;
    public static final int MOCK_ENTRY_ID = 98;
    public static final String MOCK_ENTRY_TITLE = "Active clients";
    public static final String MOCK_ENTRY_DESCRIPTION = "We can too";
    public static final String MOCK_ENTRY_PHOTO_LOCATION = "/Users/photocontest/uploads";

    public static final int MOCK_CONTEST_INVITATION_ID = 329;

}
