package com.example.photoContest.services.utils;

import com.example.photoContest.models.*;
import com.example.photoContest.models.contracts.Identify;
import com.example.photoContest.models.dto.EntryDtoOut;
import com.example.photoContest.models.dto.Login;
import org.springframework.http.HttpHeaders;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static com.example.photoContest.services.utils.Constants.*;
import static com.example.photoContest.utils.Constants.HEADER_PASSWORD;
import static com.example.photoContest.utils.Constants.HEADER_USER_NAME;

public abstract class Builder {

    public static User mockUser() {
        PersonalInfo info = mockUserPersonalInfo();
        Set<ContestEntry> set = new HashSet<>();
        User user = new User(MOCK_USER_ID, info, MOCK_USER_RANK, set);
        user.setInvitational(new HashSet<>());
        return user;
    }

    public static Organiser mockOrganiser() {
        PersonalInfo info = mockOrganiserPersonalInfo();
        return new Organiser(MOCK_ORGANISER_ID, info);
    }

    public static PersonalInfo mockUserPersonalInfo() {
        return new PersonalInfo(
                MOCK_USER_PERSONAL_INFO_ID,
                MOCK_USER_FIRST_NAME,
                MOCK_USER_LAST_NAME,
                MOCK_USER_USERNAME);
    }

    public static PersonalInfo mockOrganiserPersonalInfo() {
        return new PersonalInfo(
                MOCK_ORGANISER_PERSONAL_INFO_ID,
                MOCK_ORGANISER_FIRST_NAME,
                MOCK_ORGANISER_LAST_NAME,
                MOCK_ORGANISER_USERNAME);
    }

    public static Security mockOrganiserSecurityFromRepo() {
        return new Security(MOCK_SECURITY_ID_ORGANISER,
                MOCK_ORGANISER_USERNAME, MOCK_PASS_HASH, MOCK_SALT);
    }

    public static Security mockUserSecurityFromRepo() {
        return new Security(MOCK_SECURITY_ID_USER,
                MOCK_USER_USERNAME, MOCK_PASS_HASH, MOCK_SALT);
    }

    public static Login mockLogin() {
        return new Login(MOCK_USER_USERNAME, MOCK_PASS);
    }

    public static JuryPerson mockJuryPerson() {
        return new JuryPerson(MOCK_JURY_PERSON_ID, mockOrganiserPersonalInfo(), MOCK_CONTEST_ID, new HashSet<>());
    }

    public static Contest mockContest() {
        return new Contest(MOCK_CONTEST_ID, MOCK_CONTEST_TITLE, mockContestCategory(), MOCK_CONTEST_IS_OPEN,
                YESTERDAY, TODAY, TODAY, TOMORROW,
                Set.of(mockContestEntry()), Set.of(mockJuryPerson()), MOCK_CONTEST_IS_SCORED);
    }

    public static Contest mockContestPrePhase1() {
        Contest contest = mockContest();
        contest.setPhase1(LocalDateTime.now().plusHours(1));
        contest.setPhase2(LocalDateTime.now().plusHours(2));
        contest.setEndDate(LocalDateTime.now().plusHours(12));
        return contest;
    }

    public static Contest mockContestPhase1() {
        Contest contest = mockContest();
        contest.setPhase1(LocalDateTime.now().minusHours(3));
        contest.setPhase2(LocalDateTime.now().plusHours(1));
        contest.setEndDate(LocalDateTime.now().plusHours(12));
        return contest;
    }

    public static Contest mockContestPhase2() {
        Contest contest = mockContest();
        contest.setPhase1(LocalDateTime.now().minusHours(3));
        contest.setPhase2(LocalDateTime.now().minusHours(1));
        return contest;
    }

    public static Contest mockContestEnded() {
        Contest contest = mockContest();
        contest.setPhase1(LocalDateTime.now().minusHours(3));
        contest.setPhase2(LocalDateTime.now().minusHours(1));
        contest.setEndDate(LocalDateTime.now().minusHours(1));
        return contest;
    }

    public static ContestEntry mockContestEntry() {
        return new ContestEntry(MOCK_CONTEST_ENTRY_ID, MOCK_CONTEST_ID, MOCK_USER_ID, mockEntry());
    }

    public static Entry mockEntry() {
        return new Entry(MOCK_ENTRY_ID, new ContestEntry(), MOCK_ENTRY_TITLE,
                MOCK_ENTRY_DESCRIPTION, MOCK_ENTRY_PHOTO_LOCATION);
    }

    public static ContestCategory mockContestCategory() {
        return new ContestCategory(MOCK_CONTEST_CATEGORY_ID, MOCK_CONTEST_CATEGORY_NAME);
    }

    public static Review mockReview() {
        return new Review(MOCK_REVIEW_ID, MOCK_CONTEST_ENTRY_ID, MOCK_JURY_PERSON_ID,
                MOCK_REVIEW_TEXT, MOCK_REVIEW_SCORE);
    }

    public static HttpHeaders mockHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HEADER_PASSWORD, MOCK_PASS);
        headers.add(HEADER_USER_NAME, MOCK_USER_USERNAME);
        return headers;
    }

    public static ContestInvitation mockContestInvitation(){
        ContestInvitation invitation = new ContestInvitation();
        invitation.setContest(mockContest());
        invitation.setId(MOCK_CONTEST_INVITATION_ID);
        invitation.setUserId(MOCK_USER_ID);
        return invitation;
    }

    public static EntryDtoOut mockOutDto(){
        EntryDtoOut dto = new EntryDtoOut();
        dto.setAuthor(MOCK_USER_USERNAME);
        dto.setContestEntry(mockContestEntry());
        return dto;
    }

    public static void setIds(int id, Identify... identifies) {
        for (Identify ob : identifies) {
            ob.setId(id);
        }
    }

}
