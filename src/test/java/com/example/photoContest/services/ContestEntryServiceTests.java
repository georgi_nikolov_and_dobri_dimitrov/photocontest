package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.repositories.contracts.ContestEntryRepository;
import com.example.photoContest.repositories.contracts.ContestRepository;
import com.example.photoContest.repositories.contracts.JuryRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

import static com.example.photoContest.services.utils.Builder.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContestEntryServiceTests {

    @Mock
    ContestEntryRepository contestEntryRepository;
    @Mock
    SecurityService securityService;
    @Mock
    ContestRepository contests;
    @Mock
    JuryRepository jury;
    @Mock
    UserRepository users;

    @InjectMocks
    ContestEntryServiceImpl service;

    @Test
    public void getById_Should_Call_Repository() {
        service.getById(anyInt());

        verify(contestEntryRepository, times(1))
                .getById(anyInt());
    }

    @Test
    public void getByUserId_Should_Call_Repository() {
        service.getByUserId(anyInt());

        verify(contestEntryRepository, times(1))
                .getByUserId(anyInt());
    }

    @Test
    public void createPartial_Should_Call_Repository() {
        ContestEntry ce = mockContestEntry();
        Security security = mockUserSecurityFromRepo();
        User user = mockUser();
        Contest contest = mockContest();


        when(users.getByUserName(anyString()))
                .thenReturn(user);
        when(contests.getById(anyInt()))
                .thenReturn(contest);
        when(securityService.validateRole(security, Roles.USER))
                .thenReturn(user.getPersonalInfo());


        service.createPartial(ce, security);

        verify(contestEntryRepository, times(1))
                .create(any(ContestEntry.class));
    }

    @Test
    public void createPartial_Should_Throw_WhenUserDontHaveInvitation() {
        ContestEntry ce = mockContestEntry();
        Security security = mockUserSecurityFromRepo();
        User user = mockUser();
        Contest contest = mockContest();
        contest.setIsOpen(false);

        when(users.getByUserName(anyString()))
                .thenReturn(user);
        when(contests.getById(anyInt()))
                .thenReturn(contest);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.createPartial(ce, security));
    }
    
    @Test
    public void createPartial_Should_Throw_WhenUserHasEntryForContest() {
        ContestEntry ce = mockContestEntry();
        Security security = mockUserSecurityFromRepo();
        User user = mockUser();
        user.getContestEntries().add(mockContestEntry());
        Contest contest = mockContest();
        when(users.getByUserName(anyString()))
                .thenReturn(user);
        when(contests.getById(anyInt()))
                .thenReturn(contest);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.createPartial(ce, security));
    }

    @Test
    public void createPartial_Should_Throw_WhenIsJury() {
        ContestEntry ce = mockContestEntry();
        Security security = mockUserSecurityFromRepo();
        User user = mockUser();
        Contest contest = mockContest();
        JuryPerson juryPerson = mockJuryPerson();
        List<JuryPerson> jps = new ArrayList<>();
        jps.add(juryPerson);

        when(jury.getByUserName(anyString()))
                .thenReturn(juryPerson);
        when(jury.getByContest(anyInt()))
                .thenReturn(jps);
        when(users.getByUserName(anyString()))
                .thenReturn(user);
        when(contests.getById(anyInt()))
                .thenReturn(contest);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.createPartial(ce, security));
    }

}
