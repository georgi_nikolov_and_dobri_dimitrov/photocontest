package com.example.photoContest.services;

import com.example.photoContest.models.Organiser;
import com.example.photoContest.models.PersonalInfo;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.User;
import com.example.photoContest.models.dto.Login;
import com.example.photoContest.repositories.contracts.OrganiserRepository;
import com.example.photoContest.repositories.contracts.SecurityRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.DuplicateException;
import com.example.photoContest.utils.errors.NotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.web.server.WebSession;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import static com.example.photoContest.services.utils.Builder.*;
import static com.example.photoContest.services.utils.Constants.*;
import static com.example.photoContest.utils.Constants.*;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class SecurityServiceTests {

    @Mock
    OrganiserRepository organisers;

    @Mock
    SecurityRepository repository;

    @Mock
    UserRepository users;

    @InjectMocks
    SecurityServiceImpl service;

    @Test
    public void Login_Should_NotThrow_With_CorrectInput() {
        Login login = mockLogin();
        Security security = mockUserSecurityFromRepo();
        Mockito.when(repository.getByUserName(login.getUserName()))
                .thenReturn(security);

        Assertions.assertDoesNotThrow(() -> service.login(login));
    }

    @Test
    public void Login_Should_ThrowCorrectErr_With_WrongInput() {
        Login login = new Login(WRONG, WRONG);
        Mockito.when(repository.getByUserName(login.getUserName()))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.login(login));
    }

    @Test
    public void Login_Should_ThrowCorrectErr_With_WrongPassword() {
        Login login = new Login(MOCK_USER_USERNAME, WRONG);
        Security security = mockUserSecurityFromRepo();

        Mockito.when(repository.getByUserName(login.getUserName()))
                .thenReturn(security);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.login(login));
    }

    @Test
    public void Login_Should_ThrowCorrectErr_With_WrongUsername() {
        Login login = new Login(WRONG, MOCK_PASS);

        Mockito.when(repository.getByUserName(login.getUserName()))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.login(login));
    }

    @Test
    public void LoginHeaders_Should_NotThrow_With_CorrectInput() {
        HttpHeaders headers = mockHeaders();
        Security security = mockUserSecurityFromRepo();
        Mockito.when(repository.getByUserName(MOCK_USER_USERNAME))
                .thenReturn(security);

        Assertions.assertDoesNotThrow(() -> service.login(headers));
    }

    @Test
    public void LoginHeaders_Should_Throw_With_EmptyInput() {
        HttpHeaders headers = new HttpHeaders();
        Assertions.assertThrows(AccessDenied.class,
                () -> service.login(headers));
    }

    @Test
    public void LoginSession_Should_NotThrow_With_CorrectInput() {
        HttpSession session = new MockHttpSession();
        Login login = mockLogin();

        Security security = mockUserSecurityFromRepo();
        Mockito.when(repository.getByUserName(MOCK_USER_USERNAME))
                .thenReturn(security);

        Assertions.assertDoesNotThrow(() -> service.login(login, session));
    }

    @Test
    public void GetSecurityHeaders_Should_ReturnCorrectData_With_CorrectInput() {
        HttpHeaders headers = mockHeaders();

        Security security = mockUserSecurityFromRepo();
        Mockito.when(repository.getByUserName(MOCK_USER_USERNAME))
                .thenReturn(security);

        Security result = service.getSecurity(headers);

        Assertions.assertEquals(security, result);
    }

    @Test
    public void GetSecurityHeaders_Should_Throw_With_WrongInput() {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HEADER_PASSWORD, WRONG);
        headers.add(HEADER_USER_NAME, WRONG);

        Mockito.when(repository.getByUserName(anyString()))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.getSecurity(headers));
    }

    @Test
    public void GetSecurityHeaders_Should_Throw_With_EmptyHeaders() {
        HttpHeaders headers = new HttpHeaders();

        Assertions.assertThrows(AccessDenied.class,
                () -> service.getSecurity(headers));
    }

    @Test
    public void GetSecuritySession_Should_ReturnCorrectData_With_CorrectInput() {
        HttpSession session = new MockHttpSession();
        session.setAttribute(SESSION_NAME, MOCK_USER_USERNAME);

        Security security = mockUserSecurityFromRepo();
        Mockito.when(repository.getByUserName(anyString()))
                .thenReturn(security);

        Security result = service.getSecurity(session);

        Assertions.assertEquals(security, result);
    }

    @Test
    public void GetSecuritySession_Should_Throw_With_WrongUsername() {
        HttpSession session = new MockHttpSession();
        session.setAttribute(SESSION_NAME, WRONG);

        Mockito.when(repository.getByUserName(anyString()))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.getSecurity(session));
    }

    @Test
    public void GetSecuritySession_Should_Throw_With_EmptySession() {
        HttpSession session = new MockHttpSession();

        Assertions.assertThrows(AccessDenied.class,
                () -> service.getSecurity(session));
    }

    @Test
    public void Create_Should_ReturnCorrectResult_With_CorrectInput() {
        Security in = new Security();
        in.setUserName(MOCK_USER_USERNAME);
        in.setPassHash(MOCK_PASS);
        Security expected = mockUserSecurityFromRepo();
        Mockito.when(repository.create(Mockito.any(Security.class)))
                .thenReturn(expected);

        Security result = service.create(in);

        Assertions.assertEquals(expected, result);

    }

    @Test
    public void Create_Should_ThrowCorrectErr_With_DuplicateName() {
        Security in = new Security();
        in.setUserName(MOCK_USER_USERNAME);
        in.setPassHash(MOCK_PASS);
        Mockito.when(repository.create(Mockito.any(Security.class)))
                .thenThrow(DuplicateException.class);

        Assertions.assertThrows(DuplicateException.class,
                () -> service.create(in));
    }

    @Test
    public void ValidateRole_Should_NotThrow_With_CorrectInputForOrganiser() {
        Security security = mockOrganiserSecurityFromRepo();
        Roles role = Roles.ORGANISER;
        Organiser organiser = mockOrganiser();
        PersonalInfo expected = mockOrganiserPersonalInfo();

        Mockito.when(organisers.getByUserName(MOCK_ORGANISER_USERNAME))
                .thenReturn(organiser);
        PersonalInfo result = service.validateRole(security, role);

        Assertions.assertEquals(result, expected);
    }

    @Test
    public void ValidateRole_Should_Throw_With_WrongInputForOrganiser() {
        Security security = mockOrganiserSecurityFromRepo();
        security.setUserName(WRONG);
        Roles role = Roles.ORGANISER;

        Mockito.when(organisers.getByUserName(anyString()))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.validateRole(security, role));
    }

    @Test
    public void ValidateRole_Should_NotThrow_With_CorrectInputForUser() {
        Security security = mockUserSecurityFromRepo();
        Roles role = Roles.USER;
        User user = mockUser();
        PersonalInfo expected = mockUserPersonalInfo();

        Mockito.when(users.getByUserName(MOCK_USER_USERNAME))
                .thenReturn(user);
        PersonalInfo result = service.validateRole(security, role);

        Assertions.assertEquals(result, expected);
    }

    @Test
    public void ValidateRole_Should_Throw_With_WrongInputForUser() {
        Security security = mockUserSecurityFromRepo();
        security.setUserName(WRONG);
        Roles role = Roles.USER;

        Mockito.when(users.getByUserName(anyString()))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.validateRole(security, role));
    }

}
