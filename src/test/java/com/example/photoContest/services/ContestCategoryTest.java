package com.example.photoContest.services;

import com.example.photoContest.repositories.contracts.ContestCategoriesRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ContestCategoryTest {

    @Mock
    ContestCategoriesRepository repository;

    @InjectMocks
    ContestCategoryServiceImpl service;

    @Test
    public void getAllCategories_Should_Call_Repository() {
        service.getAllCategories();

        verify(repository, times(1))
                .getAll();
    }

    @Test
    public void getByName_Should_Call_Repository() {
        service.getByName(anyString());

        verify(repository, times(1))
                .getByName(anyString());
    }

    @Test
    public void getById_Should_Call_Repository() {
        service.getById(anyInt());

        verify(repository, times(1))
                .getById(anyInt());
    }
}
