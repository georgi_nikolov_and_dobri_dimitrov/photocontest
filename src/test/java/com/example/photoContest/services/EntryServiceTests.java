package com.example.photoContest.services;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.Entry;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.dto.EntryDtoOut;
import com.example.photoContest.repositories.contracts.ContestRepository;
import com.example.photoContest.repositories.contracts.EntryRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.services.utils.Builder;
import com.example.photoContest.utils.errors.NotOnTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import static com.example.photoContest.services.utils.Builder.*;
import static com.example.photoContest.services.utils.Constants.MOCK_ENTRY_ID;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class EntryServiceTests {


    @Mock
    EntryRepository repository;

    @Mock
    SecurityService securityService;

    @Mock
    ContestRepository contests;

    @Mock
    UserRepository users;

    @InjectMocks
    EntryServiceImpl service;

    @Test
    public void getByTitle_Should_Call_Repository(){
        service.getByTitle(anyString());

        verify(repository,times(1))
                .getByTitle(anyString());
    }

    @Test
    public void getByContestId_Should_Call_Repository(){
        service.getByContestId(anyInt());

        verify(repository,times(1))
                .getByContestId(anyInt());
    }

    @Test
    public void getById_Should_Call_Repository(){
        service.getById(anyInt());

        verify(repository,times(1))
                .getById(anyInt());
    }

    @Test
    public void getByUserId_Should_Call_Repository(){
        service.getByUser(anyInt());

        verify(repository,times(1))
                .getByUser(anyInt());
    }

    @Test
    public void getDto_Should_Return_CorrectOutput(){
        EntryDtoOut expected = mockOutDto();
        when(users.getByContestEntryId(anyInt()))
                .thenReturn(mockUser());
        when(repository.getById(anyInt()))
                .thenReturn(mockEntry());

        EntryDtoOut result = service.getDto(MOCK_ENTRY_ID);

        Assertions.assertEquals(expected.getAuthor(), result.getAuthor());
    }

    @Test
    public void create_ShouldCall_Repository(){
        Entry entry = mockEntry();
        Security security = mockUserSecurityFromRepo();
        Contest contest = mockContestPhase1();
        when(contests.getByContestEntryId(anyInt()))
                .thenReturn(contest);

        service.create(entry,security);

        verify(repository,times(1))
                .create(entry);
    }

    @Test
    public void create_Should_Throw_WhenNotOnTime_Phase2(){
        Entry entry = mockEntry();
        Security security = mockUserSecurityFromRepo();
        Contest contest = mockContestPhase2();
        when(contests.getByContestEntryId(anyInt()))
                .thenReturn(contest);

        Assertions.assertThrows(NotOnTime.class,
                ()-> service.create(entry,security));
    }

    @Test
    public void create_Should_Throw_WhenNotOnTime_NotOpen(){
        Entry entry = mockEntry();
        Security security = mockUserSecurityFromRepo();
        Contest contest = mockContestPrePhase1();
        when(contests.getByContestEntryId(anyInt()))
                .thenReturn(contest);

        Assertions.assertThrows(NotOnTime.class,
                ()-> service.create(entry,security));
    }
}
