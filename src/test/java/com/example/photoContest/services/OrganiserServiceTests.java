package com.example.photoContest.services;

import com.example.photoContest.models.Organiser;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.User;
import com.example.photoContest.repositories.contracts.OrganiserRepository;
import com.example.photoContest.repositories.contracts.PersonalInfoRepository;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.errors.DuplicateException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.photoContest.services.utils.Builder.mockOrganiser;
import static com.example.photoContest.services.utils.Builder.mockUserSecurityFromRepo;
import static com.example.photoContest.services.utils.Constants.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrganiserServiceTests {

    @Mock
    PersonalInfoRepository personalInfo;

    @Mock
    SecurityService securityService;

    @Mock
    OrganiserRepository repository;

    @InjectMocks
    OrganiserServiceImpl service;

    @Test
    public void getById_Should_CallRepository() {

        service.getById(MOCK_ORGANISER_ID);

        verify(repository, times(1))
                .getById(MOCK_ORGANISER_ID);
    }

    @Test
    public void getByUserName_Should_CallRepository() {

        service.getByUserName(MOCK_ORGANISER_USERNAME);

        verify(repository, times(1))
                .getByUserName(MOCK_ORGANISER_USERNAME);
    }

    @Test
    public void create_Should_CallRepository_With_CorrectInput() {
        Organiser organiser = mockOrganiser();
        Security security = mockUserSecurityFromRepo();
        when(personalInfo.isUnique(anyString()))
                .thenReturn(true);

        service.create(security, organiser);

        verify(repository, times(1))
                .create(organiser);
    }

    @Test
    public void create_Should_Throw_With_DuplicateUsername() {
        Organiser organiser = mockOrganiser();
        Security security = mockUserSecurityFromRepo();
        when(personalInfo.isUnique(anyString()))
                .thenReturn(false);

        Assertions.assertThrows(DuplicateException.class,
                () -> service.create(security, organiser));
    }

}
