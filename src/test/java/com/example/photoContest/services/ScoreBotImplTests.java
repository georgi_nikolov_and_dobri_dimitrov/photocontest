package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.repositories.contracts.ContestRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.photoContest.services.utils.Builder.*;
import static com.example.photoContest.services.utils.Constants.*;
import static com.example.photoContest.utils.Constants.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ScoreBotImplTests {

    private List<Contest> contestsToScore;
    private ContestEntry contestEntry;
    private final int ID = 51;
    private JuryPerson jury;
    private Contest contest;
    private User user;

    @Mock
    ContestRepository contests;

    @Mock
    UserRepository users;

    @InjectMocks
    ScoreBotImpl service;

    @BeforeEach
    void bootUp() {
        Entry entry = mockEntry();
        ContestEntry ce = mockContestEntry();
        ce.setId(ID);
        entry.setContestEntry(ce);

        Review review = mockReview();
        review.setContentEntryId(ID);
        Set<Review> reviewsForCE = new HashSet<>();
        Set<Review> reviewsForJury = new HashSet<>();
        reviewsForJury.add(review);
        reviewsForCE.add(review);

        contestEntry = mockContestEntry();
        contestEntry.setContestId(ID);
        contestEntry.setEntry(entry);
        contestEntry.setUserId(ID);
        contestEntry.setReviews(reviewsForCE);
        Set<ContestEntry> contestEntries = new HashSet<>();
        contestEntries.add(contestEntry);

        jury = mockJuryPerson();
        jury.setReviews(reviewsForJury);
        jury.setContestId(ID);
        Set<JuryPerson> juryPeople = new HashSet<>();
        juryPeople.add(jury);

        contest = mockContest();
        contest.setContestEntries(contestEntries);
        contest.setPhase2(TODAY.minusMinutes(1));
        contest.setPhase1(TODAY.minusHours(1));
        contest.setEndDate(TODAY);
        contest.setJury(juryPeople);

        user = mockUser();

        setIds(ID, entry, contestEntry, review, jury, user, contest);

        contestsToScore = List.of(contest);
    }

    @Test
    public void ScoreBot_Should_ScoreContest() {
        mockContestRepository();
        mockUserRepository();

        service.manualScoring();

        verify(contests, Mockito.times(1))
                .update(contest);
        Assertions.assertTrue(contest.getIsScored());
    }

    @Test
    public void ScoreBot_Should_ScoreUserForDominatingFirstPlace() {
        mockContestRepository();
        mockUserRepository();
        int expectedRank = user.getRank() +
                DOMINATING_FIRST_PLACE + JOINING_OPEN_CONTEST;

        service.manualScoring();

        verify(users, Mockito.times(2))
                .update(user);
        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_Should_ScoreUserForDominatingFirstPlace_Invitational() {
        mockContestRepository();
        mockUserRepository();
        contest.setIsOpen(false);
        int expectedRank = user.getRank() +
                DOMINATING_FIRST_PLACE + INVITED_IN_CONTEST;

        service.run();

        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_Should_ScoreSharedFirstPlace() {
        mockContestRepository();
        mockUserRepository();
        Mockito.when(users.getByContestEntryId(ID + 1))
                .thenReturn(mockUser());

        int expectedRank = user.getRank() + SHARED_FIRST_PLACE + JOINING_OPEN_CONTEST;
        addEntry(ID + 1, MOCK_REVIEW_SCORE);

        service.manualScoring();

        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_Should_ScoreFirstPlace_When_Second() {
        mockContestRepository();
        mockUserRepository();
        Mockito.when(users.getByContestEntryId(ID + 1))
                .thenReturn(mockUser());

        int expectedRank = user.getRank() + FIRST_PLACE + JOINING_OPEN_CONTEST;
        addEntry(ID + 1, MOCK_REVIEW_SCORE - 1);

        service.manualScoring();

        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_Should_ScoreSharedSecondPlace() {
        mockContestRepository();
        mockUserRepository();
        Mockito.when(users.getByContestEntryId(ID + 2))
                .thenReturn(mockUser());
        Mockito.when(users.getByContestEntryId(ID + 1))
                .thenReturn(mockUser());

        int expectedRank = user.getRank() + SHARED_SECOND_PLACE + JOINING_OPEN_CONTEST;
        addEntry(ID + 1, MOCK_REVIEW_SCORE);
        addEntry(ID + 2, MOCK_REVIEW_SCORE + 1);

        service.manualScoring();

        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_Should_ScoreSecondPlace() {
        mockContestRepository();
        mockUserRepository();
        Mockito.when(users.getByContestEntryId(ID + 2))
                .thenReturn(mockUser());

        int expectedRank = user.getRank() + SECOND_PLACE + JOINING_OPEN_CONTEST;
        addEntry(ID + 2, MOCK_REVIEW_SCORE + 1);

        service.manualScoring();

        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_Should_ScoreSharedThirdPlace() {
        mockContestRepository();
        mockUserRepository();
        Mockito.when(users.getByContestEntryId(ID + 3))
                .thenReturn(mockUser());
        Mockito.when(users.getByContestEntryId(ID + 2))
                .thenReturn(mockUser());
        Mockito.when(users.getByContestEntryId(ID + 1))
                .thenReturn(mockUser());

        int expectedRank = user.getRank() + SHARED_THIRD_PLACE + JOINING_OPEN_CONTEST;
        addEntry(ID + 1, MOCK_REVIEW_SCORE);
        addEntry(ID + 2, MOCK_REVIEW_SCORE + 1);
        addEntry(ID + 3, MOCK_REVIEW_SCORE + 2);

        service.manualScoring();

        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_Should_ScoreThirdPlace() {
        mockContestRepository();
        mockUserRepository();
        Mockito.when(users.getByContestEntryId(ID + 3))
                .thenReturn(mockUser());
        Mockito.when(users.getByContestEntryId(ID + 2))
                .thenReturn(mockUser());

        int expectedRank = user.getRank() + THIRD_PLACE + JOINING_OPEN_CONTEST;
        addEntry(ID + 2, MOCK_REVIEW_SCORE + 1);
        addEntry(ID + 3, MOCK_REVIEW_SCORE + 2);

        service.manualScoring();

        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_Should_ScoreCorrectly_WhenDefaultReviews() {
        mockContestRepository();
        mockUserRepository();
        Mockito.when(users.getByContestEntryId(ID + 1))
                .thenReturn(mockUser());


        int expectedRank = user.getRank() + FIRST_PLACE + JOINING_OPEN_CONTEST;
        addJuryPerson();
        addEntry(ID + 1, MOCK_REVIEW_SCORE);

        service.manualScoring();

        Assertions.assertEquals(expectedRank, user.getRank());
    }

    @Test
    public void ScoreBot_ShouldNotThrow_With_EmptyContests() {
        Mockito.when(contests.getContestsForScoring())
                .thenReturn(new ArrayList<>());

        Assertions.assertDoesNotThrow(()->service.manualScoring());
    }

    private void addJuryPerson() {
        Review review = mockReview();
        review.setContentEntryId(ID);
        contestEntry.getReviews().add(review);

        JuryPerson newJP = mockJuryPerson();
        newJP.getReviews().add(review);

        contest.getJury().add(newJP);
    }

    private void mockContestRepository() {
        Mockito.when(contests.getContestsForScoring())
                .thenReturn(contestsToScore);
    }

    private void mockUserRepository() {
        Mockito.when(users.getByContestEntryId(ID))
                .thenReturn(user);
    }

    private void addEntry(int id, int reviewScore) {
        Entry entry2 = mockEntry();
        ContestEntry ce2 = mockContestEntry();
        ce2.setId(id);
        entry2.setContestEntry(ce2);

        Review review2 = mockReview();
        review2.setContentEntryId(id);
        review2.setScore(reviewScore);
        Set<Review> reviewsForCE = new HashSet<>();
        reviewsForCE.add(review2);

        ContestEntry contestEntry2 = mockContestEntry();
        contestEntry2.setContestId(ID);
        contestEntry2.setEntry(entry2);
        contestEntry2.setReviews(reviewsForCE);

        Set<Review> reviews = jury.getReviews();
        reviews.add(review2);
        jury.setReviews(reviews);

        Set<ContestEntry> contestEntries = contest.getContestEntries();
        contestEntries.add(contestEntry2);
        contest.setContestEntries(contestEntries);

        setIds(id, entry2, contestEntry2, review2);
    }
}
