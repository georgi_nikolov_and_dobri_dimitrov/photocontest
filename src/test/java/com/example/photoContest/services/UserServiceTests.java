package com.example.photoContest.services;

import com.example.photoContest.models.User;
import com.example.photoContest.repositories.contracts.PersonalInfoRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.errors.DuplicateException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.example.photoContest.services.utils.Builder.mockUser;
import static com.example.photoContest.services.utils.Constants.MOCK_USER_ID;
import static com.example.photoContest.services.utils.Constants.MOCK_USER_USERNAME;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository repository;

    @Mock
    PersonalInfoRepository personalInfo;

    @Mock
    SecurityService securityService;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getAllUsers_Should_CallRepository() {

        service.getAllUserNames();

        verify(repository, times(1))
                .getAllUserNames();
    }

    @Test
    public void getById_Should_CallRepository() {

        service.getById(MOCK_USER_ID);

        verify(repository, times(1))
                .getById(MOCK_USER_ID);
    }

    @Test
    public void getByUserName_Should_CallRepository() {

        service.getByUserName(MOCK_USER_USERNAME);

        verify(repository, times(1))
                .getByUserName(MOCK_USER_USERNAME);
    }

    @Test
    public void create_Should_CallRepository_With_CorrectInput() {
        User user = mockUser();
        user.setId(0);
        user.getPersonalInfo().setId(0);
        when(personalInfo.isUnique(MOCK_USER_USERNAME))
                .thenReturn(true);
        when(personalInfo.create(user.getPersonalInfo()))
                .thenReturn(user.getPersonalInfo());

        service.create(user);

        verify(repository, times(1))
                .create(user);
    }

    @Test
    public void create_Should_Throw_When_UsernameIsTaken() {
        User user = mockUser();

        when(personalInfo.isUnique(MOCK_USER_USERNAME))
                .thenReturn(false);

        Assertions.assertThrows(DuplicateException.class,
                () -> service.create(user));
    }

    @Test
    public void getPlace_Should_returnCorrectPlace() {
        List<User> mok = List.of(mockUser());
        when(repository.getAll())
                .thenReturn(mok);

        int result = service.getPlace(mockUser());

        Assertions.assertEquals(1, result);
    }

}
