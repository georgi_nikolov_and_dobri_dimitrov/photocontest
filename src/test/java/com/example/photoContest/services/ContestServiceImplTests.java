package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.repositories.contracts.*;
import com.example.photoContest.services.contracts.SecurityService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.photoContest.services.utils.Builder.*;
import static com.example.photoContest.services.utils.Constants.TODAY;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceImplTests {

    @Mock
    ContestInvitationRepository invitationRepository;

    @Mock
    SecurityService securityService;

    @Mock
    OrganiserRepository organisers;

    @Mock
    ContestRepository repository;

    @Mock
    UserRepository users;

    @Mock
    JuryRepository jury;

    @InjectMocks
    ContestServiceImpl service;

    @Test
    public void getActiveForJury_Should_FilterContestsCorrectly_When_JuryHasNoReviews() {
        JuryPerson jp = mockJuryPerson();
        String userName = jp.getPersonalInfo().getUserName();
        List<Contest> active = getActiveContests();

        Mockito.when(repository.getActiveForJury(userName))
                .thenReturn(active);
        Mockito.when(jury.getByUserName(userName))
                .thenReturn(jp);

        List<Contest> result = service.getActiveForJury(userName);

        Assertions.assertEquals(2, result.size());
    }

    @Test
    public void getActiveForJury_Should_FilterContestsCorrectly_When_JuryHasReviews() {
        JuryPerson jp = mockJuryPerson();
        String userName = jp.getPersonalInfo().getUserName();
        List<Contest> active = getActiveContests();
        addContestWithReview(active, jp);

        Mockito.when(repository.getActiveForJury(userName))
                .thenReturn(active);
        Mockito.when(jury.getByUserName(userName))
                .thenReturn(jp);

        List<Contest> result = service.getActiveForJury(userName);

        Assertions.assertEquals(2, result.size());
    }

    @Test
    public void getOpenContests_Should_Call_Repository() {
        service.getOpenContests();
        verify(repository, times(1))
                .getOpenContests();
    }

    @Test
    public void getOpenForUser_Should_ReturnCorrectResult_When_NoEntries() {
        User user = mockUser();
        Contest contest = mockContestPhase1();
        contest.setContestEntries(new HashSet<>());
        List<Contest> mok = new ArrayList<>(1);
        mok.add(contest);

        when(users.getByUserName(anyString()))
                .thenReturn(user);
        when(repository.getInPhaseOne())
                .thenReturn(mok);

        List<Contest> result = service.getActiveForUser(anyString());

        Assertions.assertEquals(1, result.size());
    }

    @Test
    public void getOpenForUser_Should_ReturnCorrectResult_When_Invitational() {
        User user = mockUser();
        Contest contest = mockContestPhase1();
        contest.setContestEntries(new HashSet<>());
        contest.setIsOpen(false);
        List<Contest> mok = new ArrayList<>(1);
        mok.add(contest);

        when(users.getByUserName(anyString()))
                .thenReturn(user);
        when(repository.getInPhaseOne())
                .thenReturn(mok);

        List<Contest> result = service.getActiveForUser(anyString());

        Assertions.assertEquals(0, result.size());
    }

    @Test
    public void getOpenForUser_Should_ReturnCorrectResult_WhenUserHasEntry() {
        User user = mockUser();
        Contest contest = mockContestPhase1();
        List<Contest> mok = new ArrayList<>(1);
        mok.add(contest);

        when(users.getByUserName(anyString()))
                .thenReturn(user);
        when(repository.getInPhaseOne())
                .thenReturn(mok);

        List<Contest> result = service.getActiveForUser(anyString());

        Assertions.assertEquals(0, result.size());
    }

    private void addContestWithReview(List<Contest> active, JuryPerson jp) {
        int id = 52;
        Contest contest = mockContest();
        ContestEntry contestEntry = mockContestEntry();
        Review review = mockReview();
        setIds(id, contest, contestEntry, review);
        review.setContentEntryId(id);
        contestEntry.setContestId(id);
        contestEntry.setReviews(Set.of(review));
        contest.setContestEntries(Set.of(contestEntry));
        active.add(contest);
        jp.setReviews(Set.of(review));
    }

    private List<Contest> getActiveContests() {
        List<Contest> result = new ArrayList<>(2);
        result.add(mockContestPhase2());
        result.add(mockContestPhase2());
        return result;
    }


}
