package com.example.photoContest.services;

import com.example.photoContest.models.ContestInvitation;
import com.example.photoContest.models.Security;
import com.example.photoContest.repositories.contracts.ContestInvitationRepository;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.services.utils.Builder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.photoContest.services.utils.Builder.mockContestInvitation;
import static com.example.photoContest.services.utils.Builder.mockUserSecurityFromRepo;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class ContestInvitationsServiceTests {

    @Mock
    ContestInvitationRepository repository;

    @Mock
    SecurityService securityService;

    @InjectMocks
    ContestInvitationServiceImpl service;

    @Test
    public void create_Should_Call_Repository() {
        Security security = mockUserSecurityFromRepo();
        List<ContestInvitation> invites = new ArrayList<>();
        invites.add(mockContestInvitation());

        service.create(security, invites);

        verify(repository, times(1))
                .create(invites);
    }
}
