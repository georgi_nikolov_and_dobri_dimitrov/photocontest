package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.repositories.contracts.*;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.NotFoundException;
import com.example.photoContest.utils.errors.NotOnTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.photoContest.services.utils.Builder.*;
import static com.example.photoContest.services.utils.Constants.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class JuryServiceTests {

    @Mock
    ContestEntryRepository contestEntries;

    @Mock
    PersonalInfoRepository personalInfo;

    @Mock
    SecurityService securityService;

    @Mock
    OrganiserRepository organisers;

    @Mock
    ContestRepository contests;

    @Mock
    JuryRepository repository;

    @Mock
    ReviewRepository reviews;

    @InjectMocks
    JuryServiceImpl service;

    @Test
    public void getOptionalJury_Should_CallRepository() {
        Mockito.when(personalInfo.getOptionalJury())
                .thenReturn(List.of(mockUserPersonalInfo()));

        List<String> res = service.getOptionalJury(mockUserSecurityFromRepo());

        verify(personalInfo, times(1))
                .getOptionalJury();

        Assertions.assertEquals(List.of(MOCK_USER_USERNAME), res);
    }

    @Test
    public void createReview_Should_CallRepository_With_CorrectInput() {
        Contest contest = mockContestPhase2();
        JuryPerson juryPerson = mockJuryPerson();
        Review review = mockReview();
        when(contestEntries.getById(anyInt()))
                .thenReturn(mockContestEntry());
        when(contests.getById(anyInt()))
                .thenReturn(contest);
        when(repository.getByUserName(anyString()))
                .thenReturn(juryPerson);

        service.create(review, mockUserSecurityFromRepo());

        verify(reviews, times(1))
                .create(review);

    }

    @Test
    public void createReview_Should_Throw_When_ContestHasEnded() {
        Contest contest = mockContestEnded();
        JuryPerson juryPerson = mockJuryPerson();
        Review review = mockReview();
        when(contestEntries.getById(anyInt()))
                .thenReturn(mockContestEntry());
        when(contests.getById(anyInt()))
                .thenReturn(contest);
        when(repository.getByUserName(anyString()))
                .thenReturn(juryPerson);

        Assertions.assertThrows(NotOnTime.class,
                () -> service.create(review, mockUserSecurityFromRepo()));

    }

    @Test
    public void createReview_Should_Throw_When_Phase2IsNotOnYet() {
        Contest contest = mockContestPhase1();
        JuryPerson juryPerson = mockJuryPerson();
        Review review = mockReview();
        when(contestEntries.getById(anyInt()))
                .thenReturn(mockContestEntry());
        when(contests.getById(anyInt()))
                .thenReturn(contest);
        when(repository.getByUserName(anyString()))
                .thenReturn(juryPerson);

        Assertions.assertThrows(NotOnTime.class,
                () -> service.create(review, mockUserSecurityFromRepo()));

    }

    @Test
    public void createReview_Should_Throw_When_NotAJury() {
        Contest contest = mockContestPhase1();
        Review review = mockReview();
        when(contestEntries.getById(anyInt()))
                .thenReturn(mockContestEntry());
        when(contests.getById(anyInt()))
                .thenReturn(contest);
        when(repository.getByUserName(anyString()))
                .thenThrow(NotFoundException.class);

        Assertions.assertThrows(AccessDenied.class,
                () -> service.create(review, mockUserSecurityFromRepo()));

    }

    @Test
    public void createJury_Should_CallRepository_WhenNoOptionalJury() {
        List<Organiser> mok = List.of(mockOrganiser());
        when(organisers.getAll())
                .thenReturn(mok);

        service.createJury(mockUserSecurityFromRepo(), new ArrayList<>(), MOCK_CONTEST_ID);

        verify(repository, times(1))
                .create(Mockito.anyList());
    }

    @Test
    public void createJury_Should_CallRepository_WhenNullOptionalJury() {
        List<Organiser> mok = List.of(mockOrganiser());
        when(organisers.getAll())
                .thenReturn(mok);

        service.createJury(mockUserSecurityFromRepo(), null, MOCK_CONTEST_ID);

        verify(repository, times(1))
                .create(Mockito.anyList());
    }

    @Test
    public void createJury_Should_CallRepository_WhenOptionalJuryIsCorrect() {
        List<Organiser> mok = List.of(mockOrganiser());
        JuryPerson person = mockJuryPerson();
        List<String> names = new ArrayList<>();
        names.add(person.getPersonalInfo().getUserName());

        when(organisers.getAll())
                .thenReturn(mok);
        when(personalInfo.getByUserName(anyString()))
                .thenReturn(person.getPersonalInfo());

        service.createJury(mockUserSecurityFromRepo(),
                names,
                MOCK_CONTEST_ID);

        verify(repository, times(1))
                .create(Mockito.anyList());
    }

    @Test
    public void selfRemoveFromJury_Should_CallRepository_When_OnTime() {
        Security security = mockUserSecurityFromRepo();
        JuryPerson person = mockJuryPerson();
        PersonalInfo info = person.getPersonalInfo();
        when(securityService.validateRole(security, Roles.USER))
                .thenReturn(info);
        when(contests.getById(anyInt()))
                .thenReturn(mockContestPhase1());

        service.selfRemoveFromJury(security, MOCK_CONTEST_ID);

        verify(repository,times(1))
                .delete(Mockito.any(JuryPerson.class));
    }
    @Test
    public void selfRemoveFromJury_Should_Throw_When_NotOnTime() {
        Security security = mockUserSecurityFromRepo();
        JuryPerson person = mockJuryPerson();
        PersonalInfo info = person.getPersonalInfo();
        when(securityService.validateRole(security, Roles.USER))
                .thenReturn(info);
        when(contests.getById(anyInt()))
                .thenReturn(mockContestPhase2());

        Assertions.assertThrows(NotOnTime.class,
                ()->service.selfRemoveFromJury(security, MOCK_CONTEST_ID));
    }
}
