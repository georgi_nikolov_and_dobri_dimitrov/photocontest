package com.example.photoContest.utils;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.Review;
import com.example.photoContest.utils.errors.AccessDenied;

import java.time.LocalDateTime;
import java.util.List;

public abstract class Helpers {

    public static String notFound(String entity) {
        return String.format("%s not found", entity);
    }

    public static <E> boolean isValid(List<E> list) {
        return list != null &&
                !list.contains(null) &&
                list.size() > 0;
    }

    public static boolean isAfterPh2(Contest contest) {
        LocalDateTime today = LocalDateTime.now();
        return today.isAfter(contest.getPhase2());

    }

    public static boolean isBetweenPh2AndEnd(Contest contest) {
        LocalDateTime today = LocalDateTime.now();
        return today.isAfter(contest.getPhase2()) && today.isBefore(contest.getEndDate());
    }

    public static boolean isBetweenPh1AndPh2(Contest contest) {
        LocalDateTime today = LocalDateTime.now();
        return today.isAfter(contest.getPhase1()) && today.isBefore(contest.getPhase2());
    }

    public static Review wrongCategory(int juryId, int contestEntryId) {
        return new Review(0, contestEntryId, juryId, "Wrong Category", 0);
    }

}
