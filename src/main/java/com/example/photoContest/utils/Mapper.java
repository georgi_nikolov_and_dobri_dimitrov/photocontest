package com.example.photoContest.utils;

import com.example.photoContest.models.*;
import com.example.photoContest.models.dto.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashSet;

public abstract class Mapper {

    public static User mapUser(PersonalInfoDto dto) {
        User user = new User();
        user.setPersonalInfo(fromDto(dto));
        user.setContestEntries(new HashSet<>());
        return user;
    }

    public static JuryPerson mapJuryPerson(PersonalInfo personalInfo, int contestId) {
        JuryPerson result = new JuryPerson();
        result.setContestId(contestId);
        result.setPersonalInfo(personalInfo);
        result.setReviews(new HashSet<>());
        return result;
    }

    public static Organiser mapOrganiser(PersonalInfoDto dto) {
        Organiser organiser = new Organiser();
        organiser.setPersonalInfo(fromDto(dto));
        return organiser;
    }

    public static PersonalInfo fromDto(PersonalInfoDto dto) {
        PersonalInfo info = new PersonalInfo();
        info.setFirstName(dto.getFirstName());
        info.setLastName(dto.getLastName());
        info.setUserName(dto.getUserName());
        return info;
    }

    public static Contest mapDto(ContestDto dto, ContestCategory category) {
        Contest contest = new Contest();
        fromDto(contest, dto, category);
        return contest;
    }

    private static void fromDto(Contest contest, ContestDto dto, ContestCategory category) {

        contest.setTitle(dto.getTitle());
        contest.setContestCategory(category);
        contest.setIsOpen(dto.getIsOpen());
        contest.setCreated(LocalDateTime.now());
        contest.setPhase1(dto.getPhase1());
        contest.setPhase2(dto.getPhase2());
        contest.setEndDate(dto.getEnd());
        contest.setIsScored(false);
    }

    public static Entry fromDto(EntryDto dto) {
        Entry entry = new Entry();
        entry.setTitle(dto.getTitle());
        entry.setDescription(dto.getDescription());
        entry.setPhotoLocation("");

        return entry;
    }

    public static ContestEntry mapFullDto(ContestEntryDto dto) {
        Entry entry = fromDto(dto);
        ContestEntry result = new ContestEntry();
        result.setContestId(dto.getContestId());
        result.setEntry(entry);

        return result;
    }

    public static ContestEntry mapPartialDto(EntryDto dto) {
        Entry entry = fromDto(dto);
        ContestEntry result = new ContestEntry();
        result.setEntry(entry);

        return result;
    }

    public static Security mapSecurity(PersonalInfoDto dto) {
        Security security = new Security();
        security.setPassHash(dto.getPass());
        security.setUserName(dto.getUserName());
        return security;
    }

    public static Review fromDto(ReviewDto dto) {
        Review result = new Review();
        result.setContentEntryId(dto.getContestEntryId());
        result.setTextReview(dto.getTextReview());
        result.setScore(dto.getScore());
        return result;
    }

    public static ContestEntry contestEntryFromDto(ContestEntryDto dto) {
        ContestEntry contestEntry = new ContestEntry();
        contestEntry.setContestId(dto.getContestId());
        return contestEntry;
    }

    public static Image fromDto(ImageDto dto) throws IOException {
        Image image = new Image();
        image.setEntryId(dto.getEntryId());
        image.setPhotoBytes(dto.getPhoto().getBytes());

        return image;
    }

    public static Image fromDto(Integer entryId, MultipartFile photo) throws IOException {
        Image image = new Image();
        image.setEntryId(entryId);
        image.setPhotoBytes(photo.getBytes());

        return image;
    }

    public static ContestEntry fromString(String contestEntryDto) {
        ContestEntryDto dto = new ContestEntryDto();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            dto = objectMapper.readValue(contestEntryDto, ContestEntryDto.class);
        } catch (IOException err) {
//todo            throw new InvalidDtoDataException("Invalid data in photoDto field");
        }
        return contestEntryFromDto(dto);
    }

    public static Entry entryFromString(String entryDto) {
        EntryDto dto = new EntryDto();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            dto = objectMapper.readValue(entryDto, EntryDto.class);
        } catch (IOException err) {
//todo            throw new InvalidDtoDataException("Invalid data in photoDto field");
        }
        return fromDto(dto);
    }
}
