package com.example.photoContest.utils;

public abstract class Constants {
    public static final String SESSION_NAME = "my_name";
    public static final String SESSION_ROLE = "my_role";
    public static final String HEADER_USER_NAME = "username";
    public static final String HEADER_PASSWORD = "password";

    public static final int FIRST_NAME_MIN_LENGTH = 2;
    public static final int FIRST_NAME_MAX_LENGTH = 20;
    public static final int LAST_NAME_MIN_LENGTH = 2;
    public static final int LAST_NAME_MAX_LENGTH = 20;
    public static final int MIN_PASS_LENGTH = 8;

    public static final int JUNKIE_RANK_UP_TO = 50;
    public static final int ENTHUSIAST_RANK_UP_TO = 150;
    public static final int MASTER_RANK_UP_TO = 1000;
    public static final int JURY_MIN_RANK = 151;

    public static final String JUNKIE_TITLE = "Junkie";
    public static final String ENTHUSIAST_TITLE = "Enthusiast";
    public static final String MASTER_TITLE = "Master";
    public static final String DICTATOR_TITLE = "Wise and Benevolent Photo Dictator";

    public static final int DEFAULT_REVIEW_SCORE = 3;
    public static final int JOINING_OPEN_CONTEST = 1;
    public static final int INVITED_IN_CONTEST = 3;
    public static final int FIRST_PLACE = 50;
    public static final int SHARED_FIRST_PLACE = 40;
    public static final int SECOND_PLACE = 35;
    public static final int SHARED_SECOND_PLACE = 25;
    public static final int THIRD_PLACE = 20;
    public static final int SHARED_THIRD_PLACE = 10;
    public static final int DOMINATING_FIRST_PLACE = 75;

}
