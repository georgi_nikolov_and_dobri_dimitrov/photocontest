package com.example.photoContest.utils.errors;

public class NotOnTime extends RuntimeException{
    public NotOnTime(String message) {
        super(message);
    }
}
