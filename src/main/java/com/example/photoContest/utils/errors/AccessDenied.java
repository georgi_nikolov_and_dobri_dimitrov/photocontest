package com.example.photoContest.utils.errors;

public class AccessDenied extends RuntimeException{
    public AccessDenied() {
        super("Access denied");
    }
}
