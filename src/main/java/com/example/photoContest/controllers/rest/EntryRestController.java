package com.example.photoContest.controllers.rest;

import com.example.photoContest.models.Entry;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.dto.EntryDto;
import com.example.photoContest.services.contracts.EntryService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/api/entries")
public class EntryRestController {

    private final EntryService service;
    private final SecurityService securityService;

    @Autowired
    public EntryRestController(EntryService service, SecurityService securityService) {
        this.securityService = securityService;
        this.service = service;
    }

    @GetMapping("/search/contest/{id}")
    public List<Entry> getByContestId(@PathVariable int id) {
        return service.getByContestId(id);
    }

    @GetMapping("/{id}")
    public Entry getById(@PathVariable int id) {
        return service.getById(id);
    }

    @GetMapping("/search/user/{id}")
    public List<Entry> getByUser(@PathVariable int id) {
        return service.getByUser(id);
    }

    @GetMapping("/search")
    public Entry getByTitle(@RequestParam String title) {
        return service.getByTitle(title);
    }

    @PostMapping("/create")
    public Entry create(@Valid @RequestBody EntryDto dto, HttpHeaders headers) {
        try {
            Security security = securityService.getSecurity(headers);
            Entry entry = Mapper.fromDto(dto);
            return service.create(entry, security);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
