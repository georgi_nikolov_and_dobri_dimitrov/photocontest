package com.example.photoContest.controllers.rest;

import com.example.photoContest.models.Entry;
import com.example.photoContest.models.JuryPerson;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.User;
import com.example.photoContest.models.dto.EntryDto;
import com.example.photoContest.models.dto.PersonalInfoDto;
import com.example.photoContest.services.contracts.EntryService;
import com.example.photoContest.services.contracts.JuryService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.services.contracts.UserService;
import com.example.photoContest.utils.Mapper;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.DuplicateException;
import com.example.photoContest.utils.errors.NotFoundException;
import com.example.photoContest.utils.errors.NotOnTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import static com.example.photoContest.utils.Mapper.fromDto;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService service;
    private final JuryService juryService;
    private final EntryService entryService;
    private final SecurityService securityService;

    @Autowired
    public UserRestController(UserService service, SecurityService securityService, JuryService juryService, EntryService entryService) {
        this.securityService = securityService;
        this.entryService = entryService;
        this.juryService = juryService;
        this.service = service;
    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping("/create")
    public User create(@Valid @RequestBody PersonalInfoDto dto) {
        try {
            User user = Mapper.mapUser(dto);
            return service.create(user);
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/create/entry")
    public Entry create(@Valid @RequestBody EntryDto dto, HttpHeaders headers) {
        try {
            Security security = securityService.getSecurity(headers);
            Entry entry = fromDto(dto);
            return entryService.create(entry, security);
        } catch (AccessDenied e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (NotOnTime e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/contests/{id}/jury")
    public JuryPerson selfRemoveFromJury(@PathVariable int id, HttpHeaders headers) {
        try {
            Security security = securityService.getSecurity(headers);
            return juryService.selfRemoveFromJury(security, id);
        } catch (AccessDenied e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NotOnTime e){
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
