package com.example.photoContest.controllers.rest;

import com.example.photoContest.models.ContestEntry;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.dto.ContestEntryDto;
import com.example.photoContest.services.contracts.ContestEntryService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Mapper;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/contest_entry")
public class ContestEntryController {

    private final ContestEntryService contestEntryService;
    private final SecurityService securityService;

    public ContestEntryController(ContestEntryService contestEntryService, SecurityService securityService) {
        this.contestEntryService = contestEntryService;
        this.securityService = securityService;
    }

    @PostMapping("/create")
    public ContestEntry create(@RequestBody ContestEntryDto contestEntryDto,
                               @RequestHeader HttpHeaders headers) {
        Security security = securityService.getSecurity(headers);
        ContestEntry contestEntry = Mapper.contestEntryFromDto(contestEntryDto);

        return contestEntryService.createPartial(contestEntry, security);
    }
    @GetMapping("/{id}")
    public ContestEntry getById(@PathVariable int id) {
        return contestEntryService.getById(id);
    }
}
