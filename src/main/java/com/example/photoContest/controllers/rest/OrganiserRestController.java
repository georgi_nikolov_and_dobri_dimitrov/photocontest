package com.example.photoContest.controllers.rest;

import com.example.photoContest.models.Organiser;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.dto.PersonalInfoDto;
import com.example.photoContest.services.contracts.OrganiserService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.DuplicateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import static com.example.photoContest.utils.Mapper.mapOrganiser;

@RestController
@RequestMapping("api/organisers")
public class OrganiserRestController {

    private final OrganiserService service;
    private final SecurityService securityService;

    @Autowired
    public OrganiserRestController(OrganiserService service, SecurityService securityService) {
        this.service = service;
        this.securityService = securityService;
    }

    @GetMapping("/{id}")
    public Organiser getById(@PathVariable int id) {
        return service.getById(id);
    }

    @GetMapping("/search")
    public Organiser searchByUsername(@RequestParam String username) {
        return service.getByUserName(username);
    }

    @PostMapping("/create")
    public Organiser create(@Valid @RequestBody PersonalInfoDto dto,
                            HttpHeaders headers) {
        try {
            Security security = securityService.getSecurity(headers);
            Organiser organiser = mapOrganiser(dto);
            return service.create(security, organiser);
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AccessDenied e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
