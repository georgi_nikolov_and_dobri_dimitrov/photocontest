package com.example.photoContest.controllers.rest;

import com.example.photoContest.models.Review;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.dto.ReviewDto;
import com.example.photoContest.services.contracts.JuryService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.errors.AccessDenied;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

import static com.example.photoContest.utils.Mapper.fromDto;

@RestController
@RequestMapping("/api/jury")
public class JuryRestController {

    private final JuryService service;
    private final SecurityService securityService;

    @Autowired
    public JuryRestController(JuryService service, SecurityService securityService) {
        this.securityService = securityService;
        this.service = service;
    }

    @PostMapping("/review")
    public Review create(@Valid @RequestBody ReviewDto dto, HttpHeaders headers) {
        try {
            Security security = securityService.getSecurity(headers);
            Review review = fromDto(dto);
            return service.create(review, security);
        } catch (AccessDenied e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
