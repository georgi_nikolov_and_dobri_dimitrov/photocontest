package com.example.photoContest.controllers.rest;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.ContestCategory;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.dto.ContestDto;
import com.example.photoContest.repositories.contracts.ContestCategoriesRepository;
import com.example.photoContest.services.contracts.ContestService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.errors.DuplicateException;
import com.example.photoContest.utils.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

import static com.example.photoContest.utils.Mapper.mapDto;

@RestController
@RequestMapping("/api/contests")
public class ContestRestController {
    private final ContestService service;
    private final ContestCategoriesRepository categoriesRepository;
    private final SecurityService securityService;
    @Autowired
    public ContestRestController(ContestService service,
                                 ContestCategoriesRepository categoriesRepository,
                                 SecurityService securityService) {
        this.service = service;
        this.categoriesRepository = categoriesRepository;
        this.securityService = securityService;
    }

    @GetMapping
    public List<Contest> getAllContests() {
        return service.getAllContests();
    }

    @GetMapping("/open")
    public List<Contest> getOpenContests() {
        return service.getOpenContests();
    }

    @GetMapping("/search")
    public Contest getByTitle(@RequestParam String title) {
        return service.getByTitle(title);
    }

    @GetMapping("/{id}")
    public Contest getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping("/create")
    public Contest create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ContestDto contestDto) {
        try {
            Security security = securityService.getSecurity(headers);
            ContestCategory contestCategory = categoriesRepository.getById(contestDto.getContestCategoryId());
            Contest contest = mapDto(contestDto, contestCategory);
            return service.create(contest, security);
        } catch (DuplicateException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
