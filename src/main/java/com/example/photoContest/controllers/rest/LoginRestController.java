package com.example.photoContest.controllers.rest;

import com.example.photoContest.models.dto.Login;
import com.example.photoContest.models.Organiser;
import com.example.photoContest.models.User;
import com.example.photoContest.services.contracts.OrganiserService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.services.contracts.UserService;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/login")
public class LoginRestController {

    private final OrganiserService organisers;
    private final SecurityService service;
    private final UserService users;

    @Autowired
    public LoginRestController(SecurityService service, UserService users, OrganiserService organisers) {
        this.organisers = organisers;
        this.service = service;
        this.users = users;
    }

    @PostMapping("/user")
    public User loginUser(@Valid @RequestBody Login login) {
        try {
            service.login(login);
            return users.getByUserName(login.getUserName());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AccessDenied e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
    @PostMapping("/organiser")
    public Organiser loginOrganiser(@Valid @RequestBody Login login) {
        try {
            service.login(login);
            return organisers.getByUserName(login.getUserName());
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AccessDenied e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}