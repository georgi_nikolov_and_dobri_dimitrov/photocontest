package com.example.photoContest.controllers.rest;

import com.example.photoContest.models.Entry;
import com.example.photoContest.models.Security;
import com.example.photoContest.services.FileStorageServiceImpl;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
public class PhotoRestController {
    private static final Logger logger = LoggerFactory.getLogger(PhotoRestController.class);
    private final SecurityService securityService;
    private final FileStorageServiceImpl fileStorageService;

    @Autowired
    public PhotoRestController(SecurityService securityService, FileStorageServiceImpl fileStorageService) {
        this.securityService = securityService;
        this.fileStorageService = fileStorageService;
    }

    @PostMapping("/uploadFile")
    public Entry uploadFile(@RequestHeader HttpHeaders headers,
                                         @RequestParam("entryDto") String entryDto,
                                         @RequestParam("file") MultipartFile file) {

        Security security = securityService.getSecurity(headers);
        Entry entry = Mapper.entryFromString(entryDto);
        return fileStorageService.storeFile(file, entry, security);

//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                .path("/downloadFile/")
//                .path(fileName)
//                .toUriString();
//
//        return new UploadFileResponse(entry);
    }

    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
