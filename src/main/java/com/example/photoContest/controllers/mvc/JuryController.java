package com.example.photoContest.controllers.mvc;


import com.example.photoContest.models.Contest;
import com.example.photoContest.models.ContestEntry;
import com.example.photoContest.models.Review;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.dto.ReviewDto;
import com.example.photoContest.services.contracts.ContestEntryService;
import com.example.photoContest.services.contracts.ContestService;
import com.example.photoContest.services.contracts.JuryService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.errors.AccessDenied;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import java.util.List;

import static com.example.photoContest.controllers.mvc.Constants.ACCESS_DENIED_VIEW;
import static com.example.photoContest.controllers.mvc.Constants.CONTESTS_ATTRIBUTE;
import static com.example.photoContest.utils.Mapper.fromDto;

@Controller
@RequestMapping("/jury")
public class JuryController {

    private final SecurityService securityService;
    private final ContestService contests;
    private final JuryService juryService;
    private final ContestEntryService contestEntryService;

    @Autowired
    public JuryController(JuryService juryService, ContestService contests,
                          SecurityService securityService, ContestEntryService contestEntryService) {
        this.securityService = securityService;
        this.juryService = juryService;
        this.contests = contests;
        this.contestEntryService = contestEntryService;
    }

    @GetMapping("/review/{id}")
    public String showReviewPage(@PathVariable int id, HttpSession session, Model model) {
        try {
            Security security = securityService.getSecurity(session);
            model.addAttribute("reviewDto", new ReviewDto());
            model.addAttribute("flag", true);
            model.addAttribute("contestEntryId", id);

            return "review-page";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    @PostMapping("/review/{id}")
    public String writeReview(@PathVariable int id, @Valid @ModelAttribute("reviewDto") ReviewDto dto,
                              HttpSession session) {
        try {
            dto.setContestEntryId(id);
            Review review = getReview(dto);
            Security security = securityService.getSecurity(session);
            Review result = juryService.create(review, security);
            ContestEntry contestEntry = contestEntryService.getById(id);
            int contestId = contestEntry.getContestId();

            //todo display only entries which the organiser has not reviewed
            return "redirect:/contests/" + contestId;
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    private Review getReview(ReviewDto dto) {
        return dto.getIsValid() ? fromDto(dto) :
                new Review(0, dto.getContestEntryId(),
                        0, "Wrong category", 0);
    }

    @GetMapping("/contest/{id}/resign")
    public String selfRemoveFromJury(@PathVariable int id, HttpSession session) {
        try {
            Security security = securityService.getSecurity(session);
            juryService.selfRemoveFromJury(security, id);
            return "contest";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    @GetMapping("/contests")
    public String getActiveForJury(HttpSession session, Model model) {
        try {
            Security security = securityService.getSecurity(session);
            List<Contest> active = contests.getActiveForJury(security.getUserName());
            model.addAttribute(CONTESTS_ATTRIBUTE, active);
            return "Contests";

        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }
}
