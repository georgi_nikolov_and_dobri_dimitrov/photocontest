package com.example.photoContest.controllers.mvc;

import com.example.photoContest.models.*;
import com.example.photoContest.models.dto.EntryDto;
import com.example.photoContest.models.dto.ImageDto;
import com.example.photoContest.repositories.contracts.PhotoRepository;
import com.example.photoContest.services.contracts.*;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.NotFoundException;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.example.photoContest.controllers.mvc.Constants.*;
import static com.example.photoContest.utils.Mapper.fromDto;
import static com.example.photoContest.utils.Mapper.mapOrganiser;

@Controller
@RequestMapping("/users")
public class UserController {

    private final PhotoRepository photoRepository;
    private final SecurityService securityService;
    private final ContestService contests;
    private final EntryService entries;
    private final UserService service;
    private final ContestEntryService contestEntryService;

    @Autowired
    public UserController(PhotoRepository photoRepository, UserService service, SecurityService securityService, ContestService contests, EntryService entries, ContestEntryService contestEntryService) {
        this.photoRepository = photoRepository;
        this.securityService = securityService;
        this.contests = contests;
        this.service = service;
        this.entries = entries;
        this.contestEntryService = contestEntryService;
    }

    @GetMapping("/{id}")
    public String getById(@PathVariable int id, Model model) {
        try {
            User user = service.getById(id);
            model.addAttribute("user", user);
            return "user_view";
        } catch (NotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return NOT_FOUND_VIEW;
        }
    }

    @GetMapping("/ranking")
    public String getRankingPage(Model model) {
        List<String> usersByRank = service.getAllUserNames();

        model.addAttribute("usersByRank", usersByRank);
        return "ranking";
    }

    @GetMapping("/{id}/entries")//show user's entries
    private String showMyEntries(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = getValidUser(id, session);
            List<ContestEntry> contestEntries = contestEntryService.getByUserId(user.getId());
            model.addAttribute("contestEntries", contestEntries);
            return "users-entries";
        }catch (AccessDenied e){
            return ACCESS_DENIED_VIEW;
        }
    }

    private User getValidUser(int id, HttpSession session) {
        Security security = securityService.getSecurity(session);
        securityService.validateRole(security, Roles.USER);
        return service.getById(id);
    }

//    @GetMapping("/entry")//to remove
//    public String showCreatePage(Model model) {
//        model.addAttribute("ImageDto", new ImageDto());
//        model.addAttribute("Entry", new EntryDto());
//
//        return "scratch/photo-create";
//    }
//
//    @PostMapping("/entry")//to remove
//    public String create(@RequestParam("entryId") Integer entryId,
//                         @RequestParam("photo") MultipartFile photo, Model model) {
//        try {
//            Image image = fromDto(entryId, photo);
//            Image result = photoRepository.insertRecords(image);
//            model.addAttribute("photos", List.of(result));
//            return "/scratch/done";
//        } catch (IOException e) {
//            return "Error: " + e.getMessage();
//        }
//    }

//    @RequestMapping("/entry/{id}")//to remove
//    public void getPhoto(HttpServletResponse response, @PathVariable("id") int id)
//            throws Exception {
//        response.setContentType("image/jpeg");
//
//        Image result = photoRepository.getByEntryId(id);
//        byte[] bytes = result.getPhotoBytes();
//        InputStream inputStream = new ByteArrayInputStream(bytes);
//        IOUtils.copy(inputStream, response.getOutputStream());
//    }

    @GetMapping("/entry/{id}")
    public String getSingleEntryPage(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = getUser(session);
            Entry entry = entries.getById(id);
            ContestEntry contestEntry = contestEntryService.getById(entry.getContestEntryId());

            List<Review> reviews = new ArrayList<>(contestEntry.getReviews());

            model.addAttribute("entry", entry);
            model.addAttribute("reviews", reviews);

            return "single-entry-page";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    @GetMapping("/profile")
    public String showProfilePage(HttpSession session, Model model) {
        try {
            User user = getUser(session);
            model.addAttribute("user", user);
            int place = service.getPlace(user);
            model.addAttribute("place", place);

            List<ContestEntry> userEntries = contestEntryService.getByUserId(user.getId());
            model.addAttribute("userEntries", userEntries);

            if(user.getRank() > 151) {
                List<Contest> userJuryContests = contests.getActiveForJury(user.getUserName());
                model.addAttribute("contests", userJuryContests);
            }else
                model.addAttribute("contests", new ArrayList<>());


            return "profile-user";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    private User getUser(HttpSession session) {
        Security security = securityService.getSecurity(session);
        return service.getByUserName(security.getUserName());
    }


   /* private void setEntries(Model model, int userId) {
        List<Entry> user_s;
        try {
            user_s = entries.getByUser(userId);
        } catch (NotFoundException e) {
            user_s = new ArrayList<>();
        }
        model.addAttribute("entries", user_s);
    }

    private void setJuryContests(Model model, String username) {
        List<Contest> forJury;
        try {
            forJury = contests.getActiveForJury(username);
        } catch (EntityNotFoundException e) {
            forJury = new ArrayList<>();
        }
        model.addAttribute("juryContests", forJury);
    }

    private void setPrivateContests(Model model, String username) {
        List<Contest> invitational;
        try {
            invitational = contests.getActiveForUser(username).stream()
                    .filter(Contest::getIsOpen)
                    .collect(Collectors.toList());
        } catch (NotFoundException e) {
            invitational = new ArrayList<>();
        }
        model.addAttribute("privateContests", invitational);
    }*/
}
