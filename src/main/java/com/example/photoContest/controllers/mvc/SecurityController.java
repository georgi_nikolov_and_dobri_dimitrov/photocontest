package com.example.photoContest.controllers.mvc;


import com.example.photoContest.models.Security;
import com.example.photoContest.models.User;
import com.example.photoContest.models.dto.Login;
import com.example.photoContest.models.dto.PersonalInfoDto;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.services.contracts.UserService;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.DuplicateException;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.example.photoContest.controllers.mvc.Constants.*;
import static com.example.photoContest.utils.Mapper.mapSecurity;
import static com.example.photoContest.utils.Mapper.mapUser;

@Controller
@RequestMapping("/")
public class SecurityController {

    private final SecurityService securityService;
    private final UserService users;

    @Autowired
    public SecurityController(SecurityService securityService, UserService users) {
        this.securityService = securityService;
        this.users = users;
    }

    @GetMapping
    public String getHomePage(){
        return INDEX_VIEW;
    }

    @GetMapping(LOGIN_URL)
    public String getLoginPage(Model model, HttpSession session) {
        setLoginTo(model, session);
        return LOGIN_VIEW;
    }

    @PostMapping(LOGIN_URL)
    public String login(@Valid @ModelAttribute(LOGIN_ATTRIBUTE) Login login, HttpSession session,
                        BindingResult errors) {
        try {
            if (errors.hasErrors())
                return LOGIN_VIEW;
            securityService.login(login, session);
            return "redirect:/";
        } catch (AccessDenied e) {
            errors.rejectValue("userName", "userName.Password", "Wrong Username or Password");
            return LOGIN_VIEW;
        }
    }

    @GetMapping(REGISTER_URL)
    public String getRegisterPage(Model model) {
        model.addAttribute(PERSONAL_INFO_ATTRIBUTE, new PersonalInfoDto());
        return REGISTER_VIEW;
    }

    @PostMapping(REGISTER_URL)
    public String register(@Valid @ModelAttribute(PERSONAL_INFO_ATTRIBUTE) PersonalInfoDto dto,
                           HttpSession session, BindingResult errors) {
        try {
            if (errors.hasErrors())
                return REGISTER_VIEW;
            Security security = mapSecurity(dto);
            securityService.create(security);

            User user = mapUser(dto);
            User created = users.create(user);
            securityService.setSession(created, session);

            return REDIRECT_HOME;
        }catch (DuplicateException e){
            errors.rejectValue("userName", "userName.exists", e.getMessage());
            return REGISTER_VIEW;
        }
    }

    @GetMapping("logout")
    public String logout(HttpSession session){
        securityService.logout(session);
        return REDIRECT_HOME;
    }


    private void setLoginTo(Model model, HttpSession session) {
        model.addAttribute(LOGIN_ATTRIBUTE,
                session.getAttribute(LOGIN_ATTRIBUTE) == null ?
                        new Login() :
                        session.getAttribute(LOGIN_ATTRIBUTE));
    }
}
