package com.example.photoContest.controllers.mvc;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.ContestCategory;
import com.example.photoContest.models.ContestEntry;
import com.example.photoContest.models.Entry;

import java.util.HashSet;

public abstract class Constants {

    public static final String PERSONAL_INFO_ATTRIBUTE = "personalInfoDto";
    public static final String CONTEST_DTO_ATTRIBUTE = "contestDto";
    public static final String CONTEST_ATTRIBUTE = "contest";
    public static final String CONTESTS_ATTRIBUTE = "contests";
    public static final String CONTEST_CATEGORIES_ATTRIBUTE = "contestCategories";
    public static final String CONTEST_OPTIONAL_JURY_ATTRIBUTE = "optionalJury";
    public static final String INVITED_USERS_ATTRIBUTE = "invitedUsers";
    public static final String ERROR_ATTRIBUTE = "error";
    public static final String CONTEST_ACTIVE_FOR_JURY = "active_for_jury";
    public static final String CONTEST_ACTIVE_FOR_USERS = "active_for_users";
    public static final String LOGIN_ATTRIBUTE = "login";

    public static final String CONTEST_CREATE_VIEW = "contest-create";
    public static final String CONTEST_ENTRY_SUBMIT = "contest-entry-submit";
    public static final String CONTEST_VIEW = "contest_by_id";
    public static final String LOGIN_VIEW = "login";
    public static final String ERROR_VIEW = "error";
    public static final String ACCESS_DENIED_VIEW = "access-denied";
    public static final String NOT_FOUND_VIEW = "not-found";
    public static final String REGISTER_VIEW = "register";
    public static final String INDEX_VIEW = "index";
    public static final String REDIRECT_HOME = "redirect:/";

    public static final String CREATE_URL = "/new";
    public static final String GET_BY_ID_URL = "/{id}";
    public static final String REGISTER_URL = "register";
    public static final String LOGIN_URL = "login";
    public static final Contest EMPTY = new Contest(0, "No Contests", new ContestCategory(),
            true, null, null, null, null,
            new HashSet<>(), new HashSet<>(), true);
}
