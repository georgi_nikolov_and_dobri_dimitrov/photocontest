package com.example.photoContest.controllers.mvc;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.Security;
import com.example.photoContest.services.contracts.ContestService;
import com.example.photoContest.services.contracts.OrganiserService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

import java.util.List;

import static com.example.photoContest.controllers.mvc.Constants.*;

@Controller
@RequestMapping("/organisers")
public class OrganiserController {

    private final OrganiserService service;
    private final ContestService contests;
    private final SecurityService securityService;

    @Autowired
    public OrganiserController(OrganiserService service, ContestService contests, SecurityService securityService) {
        this.securityService = securityService;
        this.contests = contests;
        this.service = service;
    }

    @GetMapping("/profile")
    public String showProfilePage(HttpSession session, Model model) {
        try {
            Security security = securityService.getSecurity(session);
            securityService.validateRole(security, Roles.ORGANISER);
            setContests(model, security);

            return "profile-organiser";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    private void setContests(Model model, Security security) {
        List<Contest> active = contests.getActiveForJury(security.getUserName());
//        while (active.size() < 2)
//            active.add(EMPTY);
        model.addAttribute(CONTESTS_ATTRIBUTE, active);
    }
}
