package com.example.photoContest.controllers.mvc;


import com.example.photoContest.models.*;
import com.example.photoContest.models.dto.ContestDto;
import com.example.photoContest.services.contracts.*;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.DuplicateException;
import com.example.photoContest.utils.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

import java.time.Instant;
import java.time.ZoneId;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.photoContest.controllers.mvc.Constants.*;
import static com.example.photoContest.utils.Helpers.isValid;
import static com.example.photoContest.utils.Mapper.mapDto;

@Controller
@RequestMapping("/contests")
public class ContestController {

    private final UserService users;
    private final OrganiserService organisers;
    private final ContestService service;
    private final JuryService juryService;
    private final ContestService contestService;
    private final SecurityService securityService;
    private final ContestCategoryService categories;
    private final ContestEntryService contestEntryService;
    private final FileStorageService fileStorageService;

    @Autowired
    public ContestController(ContestService service, SecurityService securityService, ContestCategoryService categories,
                             JuryService juryService, UserService users, OrganiserService organisers, ContestService contestService, ContestEntryService contestEntryService, FileStorageService fileStorageService) {
        this.securityService = securityService;
        this.organisers = organisers;
        this.contestService = contestService;
        this.juryService = juryService;
        this.categories = categories;
        this.service = service;
        this.users = users;
        this.contestEntryService = contestEntryService;
        this.fileStorageService = fileStorageService;
    }

    @GetMapping("/organisers")
    public String showContestPageForOrganisers(Model model, HttpSession session) {
        try {
//            List<Contest> active = getContests(session);
            List<Contest> allContests = contestService.getAllContests();
            Contest firstContest = allContests.get(0);

            allContests.remove(0);
            model.addAttribute("firstContest", firstContest);
            model.addAttribute(CONTESTS_ATTRIBUTE, allContests);

            return "contests-organiser";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    @GetMapping("/users")
    public String showContestPageForUsers(Model model, HttpSession session) {
        try {
            List<Contest> openContests = contestService.getOpenContests();
            model.addAttribute(CONTESTS_ATTRIBUTE, openContests);

            return "Contests";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    @GetMapping("/{id}")
    public String showSingleContestPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            Security security = securityService.getSecurity(session);
            try {
                User user = users.getByUserName(security.getUserName());
                checkForEntries(user, id, model);
            } catch (NotFoundException e) {
                Organiser organiser = organisers.getByUserName(security.getUserName());
            }
//            List<Contest> active = getContests(session);
            Contest contest = contestService.getById(id);
            Set<ContestEntry> contestEntries = contest.getContestEntries();

            Instant instant;
            String currentPhaseName = contest.getCurrentPhase();
            switch (currentPhaseName) {
                case "Pre-open":
                    instant = contest.getPhase1().atZone(ZoneId.of("Europe/Sofia")).toInstant();
                    model.addAttribute("currentPhase", instant);
                    break;
                case "Phase 1":
                    instant = contest.getPhase2().atZone(ZoneId.of("Europe/Sofia")).toInstant();
                    model.addAttribute("currentPhase", instant);
                    break;
                case "Phase 2":
                    instant = contest.getEndDate().atZone(ZoneId.of("Europe/Sofia")).toInstant();
                    model.addAttribute("currentPhase", instant);
                    break;
            }

            model.addAttribute("contest", contest);
            model.addAttribute("contestEntries", contestEntries);
            model.addAttribute("currentPhaseName", currentPhaseName);

            return "contest";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    @GetMapping("/invitationals")
    public String getInvitationalsView(HttpSession session, Model model) {
        try {
            Security security = securityService.getSecurity(session);
            List<Contest> contestJuryInvites = contestService.getAllContestsForJuryUser(security.getUserName());
            List<Contest> contestInvites = contestService.getAllContestInvitesForUser(security.getUserName());

            model.addAttribute("juryInvites", contestJuryInvites);
            model.addAttribute("invites", contestInvites);

            return "invitationals";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }

    }

    @GetMapping("/{id}/enroll")
    public String createContestEntry(@PathVariable int id, HttpSession session, Model model) {
        try {
            Security security = securityService.getSecurity(session);

            Contest contest = contestService.getById(id);
            User user = users.getByUserName(security.getUserName());

            ContestEntry contestEntry = new ContestEntry();
            contestEntry.setContestId(id);
            contestEntry.setUserId(user.getId());

            contestEntryService.createPartial(contestEntry, security);

            return "redirect:/contests/" + id + "/entry/create";
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        } catch (NotFoundException e) {
            return NOT_FOUND_VIEW;
        }
    }

    @GetMapping("/{id}/entry/create")
    public String getEntryCreationView(@PathVariable int id, HttpSession session, Model model) {
        User user = getUser(session);

        Contest contest = contestService.getById(id);

        model.addAttribute("contest", contest);
        model.addAttribute("user", user);

        return "contest-entry-submit";
    }

    @PostMapping("/{id}/entry/create")
    public String createEntry(@PathVariable int id, @RequestParam("file") MultipartFile file,
                              @RequestParam("title") String title,
                              @RequestParam("description") String description,
                              HttpSession session, Model model) {
        try {
            Security security = securityService.getSecurity(session);

            Contest contest = contestService.getById(id);
            Set<ContestEntry> contestEntries = contest.getContestEntries();
            User user = users.getByUserName(security.getUserName());

            ContestEntry fromRepo = contestEntries.stream()
                    .filter(contestEntry -> contestEntry.getUserId() == user.getId())
                    .findFirst()
                    .orElse(null);

            Entry entry = new Entry();
            entry.setContestEntry(fromRepo);
            entry.setTitle(title);
            entry.setDescription(description);
            entry.setPhotoLocation("");

            ContestEntry contestEntry = contestEntryService.getById(fromRepo.getId());
            fileStorageService.storeFile(file, entry, security);

            return REDIRECT_HOME;
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    private User getUser(HttpSession session) {
        Security security = securityService.getSecurity(session);
        return users.getByUserName(security.getUserName());
    }

    @GetMapping(CREATE_URL)
    public String showCreatePage(Model model, HttpSession session) {
        try {
            Security security = securityService.getSecurity(session);
            securityService.validateRole(security, Roles.ORGANISER);

            setModelForCreate(model, security, new ContestDto());
            return CONTEST_CREATE_VIEW;
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    @PostMapping(CREATE_URL)
    public String create(@ModelAttribute(CONTEST_DTO_ATTRIBUTE) ContestDto dto,
                         BindingResult errors, HttpSession session, Model model) {
        try {
            Security security = securityService.getSecurity(session);
            if (errors.hasErrors()) {
                setModelForCreate(model, security, dto);
                return CONTEST_CREATE_VIEW;
            }
            Contest result = create(dto, security);
            return REDIRECT_HOME;

        } catch (NotFoundException e) {
            model.addAttribute(ERROR_ATTRIBUTE, e.getMessage());
            return NOT_FOUND_VIEW;
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        } catch (DuplicateException e) {
            setModelForCreate(model, securityService.getSecurity(session), dto);
            errors.rejectValue("title", "title.exists", e.getMessage());
            return CONTEST_CREATE_VIEW;
        }
    }


    //    @GetMapping(GET_BY_ID_URL)
//    public String showContest(@PathVariable int id, Model model) {
//        try {
//            Contest contest = service.getById(id);
//            model.addAttribute(CONTEST_ATTRIBUTE, contest);
//            return CONTEST_VIEW;
//        } catch (NotFoundException e) {
//            model.addAttribute(ERROR_ATTRIBUTE, e.getMessage());
//            return ERROR_VIEW;
//        }
//    }
    @GetMapping("/active/jury")
    public String showActiveForJury(Model model, HttpSession session) {
        try {
            Security security = securityService.getSecurity(session);
            model.addAttribute(CONTESTS_ATTRIBUTE,
                    service.getActiveForJury(security.getUserName()));

            return CONTEST_ACTIVE_FOR_JURY;
        } catch (AccessDenied e) {
            return ACCESS_DENIED_VIEW;
        }
    }

    private List<Contest> getContests(HttpSession session) {
        Security security = securityService.getSecurity(session);
        return service.getActiveForUser(security.getUserName());
    }

    private Contest create(ContestDto dto, Security security) {
        ContestCategory category = categories.getById(dto.getContestCategoryId());
        Contest result = service.create(mapDto(dto, category), security);

        List<JuryPerson> jury = juryService.createJury(security,
                dto.getOptionalJuryUsernames(), result.getId());
        result.setJury(new HashSet<>(jury));

        if (!result.isOpen() && isValid(dto.getInvitedUsernames()))
            service.sendInvitations(security, dto.getInvitedUsernames(), result.getId());
        return result;
    }

    private void setModelForCreate(Model model, Security security, ContestDto dto) {
        model.addAttribute(CONTEST_DTO_ATTRIBUTE, dto);
        model.addAttribute(INVITED_USERS_ATTRIBUTE, users.getAllUserNames());
        model.addAttribute(CONTEST_CATEGORIES_ATTRIBUTE, categories.getAllCategories());
        model.addAttribute(CONTEST_OPTIONAL_JURY_ATTRIBUTE, juryService.getOptionalJury(security));
    }

    private String viewById(Contest result) {
        return String.format("redirect:/contests/%d", result.getId());
    }

    private void hasEntry(Model model, ContestEntry contestEntry) {
        model.addAttribute("hasEntry",
                contestEntry.getEntry() != null);
    }

    private void checkForEntries(User user, int contestId, Model model) {
        List<ContestEntry> entries = contestEntryService.getByUserId(user.getId());
        ContestEntry contestEntry = (entries.stream()
                .filter(ce -> ce.getContestId() == contestId)
                .findFirst()
                .orElse(null));
        model.addAttribute("isEnrolled", contestEntry != null);

        if (contestEntry != null)
            hasEntry(model, contestEntry);
        model.addAttribute("hasEntry", false);
    }
}
