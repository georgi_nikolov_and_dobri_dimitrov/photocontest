package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "jury")
public class JuryPerson implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "jury_id")
    private int juryId;

    @OneToOne
    @JoinColumn(name = "personal_info_id")
    private PersonalInfo personalInfo;

    @Column(name = "contest_id")
    private int contestId;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "author")
    private Set<Review> reviews;

    public JuryPerson() {
    }

    public JuryPerson(int juryId, PersonalInfo personalInfo,
                      int contestId, Set<Review> reviews) {
        this.juryId = juryId;
        this.personalInfo = personalInfo;
        this.contestId = contestId;
        this.reviews = reviews;
    }

    @Override
    public int getId() {
        return juryId;
    }

    @Override
    public void setId(int juryId) {
        this.juryId = juryId;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JuryPerson that = (JuryPerson) o;
        return juryId == that.juryId && contestId == that.contestId && personalInfo.equals(that.personalInfo) && Objects.equals(reviews, that.reviews);
    }

    @Override
    public int hashCode() {
        return Objects.hash(juryId, personalInfo, contestId);
    }
}
