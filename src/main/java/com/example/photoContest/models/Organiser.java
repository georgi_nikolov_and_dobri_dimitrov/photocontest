package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "organisers")
public class Organiser implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "organiser_id")
    private int organiserId;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "personal_Info_id")
    private PersonalInfo personalInfo;

    public Organiser() {
    }

    public Organiser(int organiserId, PersonalInfo personalInfo) {
        this.organiserId = organiserId;
        this.personalInfo = personalInfo;
    }

    @Override
    public int getId() {
        return organiserId;
    }

    @Override
    public void setId(int id) {
        this.organiserId = id;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public String getFirstName(){
        return personalInfo.getFirstName();
    }

    public String getLastName(){
        return personalInfo.getLastName();
    }

    public String getUserName(){
        return personalInfo.getUserName();
    }

}
