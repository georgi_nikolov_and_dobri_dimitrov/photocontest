package com.example.photoContest.models;

public class UploadFileResponse {
    private Entry entry;

    public UploadFileResponse(Entry entry) {
        this.entry = entry;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }
}
