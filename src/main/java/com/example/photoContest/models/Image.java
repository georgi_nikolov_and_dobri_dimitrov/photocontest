package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;

import javax.persistence.*;

@Entity
@Table(name = "photos")
public class Image implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "photo_id")
    private int photoId;

    @Column(name = "photo")
    private byte[] photoBytes;

    @Column(name = "entry_id")
    private int entryId;

    public Image() {
    }

    public Image(int id, byte[] photoBytes, int entryId) {
        this.photoId = id;
        this.photoBytes = photoBytes;
        this.entryId = entryId;
    }

    @Override
    public int getId() {
        return photoId;
    }

    @Override
    public void setId(int photoId) {
        this.photoId = photoId;
    }

    public byte[] getPhotoBytes() {
        return photoBytes;
    }

    public void setPhotoBytes(byte[] photoBytes) {
        this.photoBytes = photoBytes;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }
}
