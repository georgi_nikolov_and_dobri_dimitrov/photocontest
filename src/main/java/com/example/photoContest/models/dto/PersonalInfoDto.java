package com.example.photoContest.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.example.photoContest.utils.Constants.*;

public class PersonalInfoDto {

    @NotNull
    @Size(min = FIRST_NAME_MIN_LENGTH, max = FIRST_NAME_MAX_LENGTH)
    private String firstName;

    @NotNull
    @Size(min = LAST_NAME_MIN_LENGTH, max = LAST_NAME_MAX_LENGTH)
    private String lastName;

    @NotNull
    @NotBlank
    private String userName;

    @NotNull
    @Size(min = MIN_PASS_LENGTH)
    private String pass;

    public PersonalInfoDto() {
    }

    public PersonalInfoDto(@NotNull @Size(min = FIRST_NAME_MIN_LENGTH, max = FIRST_NAME_MAX_LENGTH) String firstName,
                           @NotNull @Size(min = LAST_NAME_MIN_LENGTH, max = LAST_NAME_MAX_LENGTH) String lastName,
                           @NotNull @NotBlank String userName,
                           @NotNull @Size(min = MIN_PASS_LENGTH) String pass) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.pass = pass;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
