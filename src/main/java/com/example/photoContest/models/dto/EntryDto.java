package com.example.photoContest.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

public class EntryDto {

    @Positive
    private int contestEntryId;

    @NotBlank
    private String title;

    @NotBlank
    private String description;

    @NotBlank
    private String photoLocation;

    public EntryDto() {
    }

    public EntryDto(@Positive int contestEntryId, @NotBlank String title,
                    @NotBlank String description, @NotBlank String photoLocation) {
        this.contestEntryId = contestEntryId;
        this.title = title;
        this.description = description;
        this.photoLocation = photoLocation;
    }

    public int getContestEntryId() {
        return contestEntryId;
    }

    public void setContestEntryId(int contestEntryId) {
        this.contestEntryId = contestEntryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoLocation() {
        return photoLocation;
    }

    public void setPhotoLocation(String photoLocation) {
        this.photoLocation = photoLocation;
    }
}
