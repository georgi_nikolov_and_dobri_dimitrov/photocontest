package com.example.photoContest.models.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

public class ImageDto {

    @Positive
    private int entryId;

    @NotBlank
    private MultipartFile photo;

    public ImageDto() {
    }

    public ImageDto(@Positive int entryId, @NotBlank MultipartFile photo) {
        this.entryId = entryId;
        this.photo = photo;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }
}
