package com.example.photoContest.models.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

public class ContestEntryDto extends EntryDto{

    @Positive
    private int contestId;

    public ContestEntryDto() {
    }

    public ContestEntryDto(@Positive int contestEntryId, @NotBlank String title,
                           @NotBlank String description, @NotBlank String photoLocation,
                           @Positive int contestId) {
        super(contestEntryId, title, description, photoLocation);
        this.contestId = contestId;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }
}
