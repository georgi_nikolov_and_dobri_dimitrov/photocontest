package com.example.photoContest.models.dto;

import java.util.List;

public class ContestInvitationsDto {

    private int contestId;

    private List<String> userNames;

    public ContestInvitationsDto(int contestId, List<String> userNames) {
        this.contestId = contestId;
        this.userNames = userNames;
    }

    public ContestInvitationsDto() {
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public List<String> getUserNames() {
        return userNames;
    }

    public void setUserNames(List<String> userNames) {
        this.userNames = userNames;
    }
}
