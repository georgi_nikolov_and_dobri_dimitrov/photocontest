package com.example.photoContest.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static com.example.photoContest.utils.Constants.MIN_PASS_LENGTH;

public class Login {

    @NotBlank
    private String userName;

    @NotBlank
    @Size(min = MIN_PASS_LENGTH)
    private String passWord;

    public Login() {
    }

    public Login(@NotBlank String userName,
                 @NotBlank @Size(min = MIN_PASS_LENGTH) String passWord) {
        this.userName = userName;
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
