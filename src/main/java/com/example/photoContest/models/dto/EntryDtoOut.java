package com.example.photoContest.models.dto;

import com.example.photoContest.models.Entry;

public class EntryDtoOut extends Entry {

    private String author;

    public EntryDtoOut() {
    }

/*    public EntryDtoOut(int entryId, int contestEntryId, String title, String description, String photoLocation, String author) {
        super(entryId, contestEntryId, title, description, photoLocation);
        this.author = author;
    }*/

    public EntryDtoOut(Entry entry, String author) {
        super(entry.getId(), entry.getContestEntry(), entry.getTitle(), entry.getDescription(), entry.getPhotoLocation());
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
