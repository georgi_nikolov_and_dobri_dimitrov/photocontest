package com.example.photoContest.models.dto;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class ReviewDto {

    private int contestEntryId;

    private String textReview;

    @PositiveOrZero
    private int score;

    private boolean isValid;

    public ReviewDto() {
    }

    public ReviewDto(int contestEntryId,
                     String textReview,
                     int score, boolean isValid) {
        this.contestEntryId = contestEntryId;
        this.textReview = textReview;
        this.score = score;
        this.isValid = isValid;
    }

    public int getContestEntryId() {
        return contestEntryId;
    }

    public void setContestEntryId(int contestEntryId) {
        this.contestEntryId = contestEntryId;
    }

    public String getTextReview() {
        return textReview;
    }

    public void setTextReview(String textReview) {
        this.textReview = textReview;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean getIsValid() {
        return isValid;
    }

    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }
}
