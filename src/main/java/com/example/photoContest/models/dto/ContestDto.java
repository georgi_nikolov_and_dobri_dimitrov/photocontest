package com.example.photoContest.models.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public class ContestDto {

    @NotNull
    private String title;

    @NotNull
    private int contestCategoryId;

    @NotNull
    private boolean isOpen;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Future(message = "Mast be future date")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @NotNull
    private LocalDateTime phase1;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Future(message = "Mast be future date")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @NotNull
    private LocalDateTime phase2;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Future(message = "Mast be future date")
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @NotNull
    private LocalDateTime end;

    private List<String> invitedUsernames;

    private List<String> OptionalJuryUsernames;

    public ContestDto() {
    }

    public ContestDto(String title, int contestCategoryId,
                      boolean isOpen, LocalDateTime phase1,
                      LocalDateTime phase2, LocalDateTime end) {
        this.title = title;
        this.contestCategoryId = contestCategoryId;
        this.isOpen = isOpen;
        this.phase1 = phase1;
        this.phase2 = phase2;
        this.end = end;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getContestCategoryId() {
        return contestCategoryId;
    }

    public void setContestCategoryId(int contestCategoryId) {
        this.contestCategoryId = contestCategoryId;
    }

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public LocalDateTime getPhase1() {
        return phase1;
    }

    public void setPhase1(LocalDateTime phase1) {
        this.phase1 = phase1;
    }

    public LocalDateTime getPhase2() {
        return phase2;
    }

    public void setPhase2(LocalDateTime phase2) {
        this.phase2 = phase2;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public List<String> getInvitedUsernames() {
        return invitedUsernames;
    }

    public void setInvitedUsernames(List<String> invitedUsernames) {
        this.invitedUsernames = invitedUsernames;
    }

    public List<String> getOptionalJuryUsernames() {
        return OptionalJuryUsernames;
    }

    public void setOptionalJuryUsernames(List<String> optionalJuryUsernames) {
        OptionalJuryUsernames = optionalJuryUsernames;
    }
}
