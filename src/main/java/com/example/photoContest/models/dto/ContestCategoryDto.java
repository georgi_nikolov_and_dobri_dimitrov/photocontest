package com.example.photoContest.models.dto;

import javax.validation.constraints.NotNull;

public class ContestCategoryDto {

    @NotNull
    private String name;

    public ContestCategoryDto() {
    }

    public ContestCategoryDto(@NotNull String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
