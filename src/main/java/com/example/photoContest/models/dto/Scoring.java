package com.example.photoContest.models.dto;

public class Scoring {

    private int contestEntryId;
    private double score;

    public Scoring() {
    }

    public Scoring(int contestEntryId, double score) {
        this.contestEntryId = contestEntryId;
        this.score = score;
    }

    public int getContestEntryId() {
        return contestEntryId;
    }

    public void setContestEntryId(int contestEntryId) {
        this.contestEntryId = contestEntryId;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
