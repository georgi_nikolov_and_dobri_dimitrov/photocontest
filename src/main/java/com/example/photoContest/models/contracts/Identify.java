package com.example.photoContest.models.contracts;

public interface Identify {
    int getId();

    void setId(int id);
}
