package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "reviews")
public class Review implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "review_id")
    private int reviewId;

    @Column(name = "contest_entry_id")
    private int contentEntryId;

    @Column(name = "author")
    private int author;

    @Column(name = "text_review")
    private String textReview;

    @Column(name = "score")
    private int score;

    public Review() {
    }

    public Review(int reviewId, int contentEntryId, int author, String textReview, int score) {
        this.reviewId = reviewId;
        this.contentEntryId = contentEntryId;
        this.author = author;
        this.textReview = textReview;
        this.score = score;
    }

    @Override
    public int getId() {
        return reviewId;
    }

    @Override
    public void setId(int reviewId) {
        this.reviewId = reviewId;
    }

    public int getContentEntryId() {
        return contentEntryId;
    }

    public void setContentEntryId(int contentId) {
        this.contentEntryId = contentId;
    }

    public int getAuthor() {
        return author;
    }

    public void setAuthor(int author) {
        this.author = author;
    }

    public String getTextReview() {
        return textReview;
    }

    public void setTextReview(String textReview) {
        this.textReview = textReview;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return reviewId == review.reviewId && contentEntryId == review.contentEntryId && author == review.author && score == review.score && Objects.equals(textReview, review.textReview);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reviewId, contentEntryId, author, textReview, score);
    }
}
