package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "contest_categories")
public class ContestCategory implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_category_id")
    private int id;

    @Column(name = "name")
    private String name;

    public ContestCategory(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public ContestCategory() {
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContestCategory that = (ContestCategory) o;
        return id == that.id && name.equals(that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
