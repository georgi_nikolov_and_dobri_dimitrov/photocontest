package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "security")
public class Security implements Identify {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "security_id")
    private int securityId;

    @Column(name = "user_name")
    private String userName;

    @JsonIgnore
    @Column(name = "pass_hash")
    private String passHash;

    @JsonIgnore
    @Column(name = "salt")
    private double salt;

    public Security() {
    }

    public Security(int securityId, String userName, String passHash, double salt) {
        this.securityId = securityId;
        this.userName = userName;
        this.passHash = passHash;
        this.salt = salt;
    }

    @Override
    public int getId() {
        return securityId;
    }

    @Override
    public void setId(int securityId) {
        this.securityId = securityId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassHash() {
        return passHash;
    }

    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }

    public double getSalt() {
        return salt;
    }

    public void setSalt(double salt) {
        this.salt = salt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Security security = (Security) o;
        return securityId == security.securityId &&
                Double.compare(security.salt, salt) == 0 &&
                userName.equals(security.userName) &&
                passHash.equals(security.passHash);
    }

    @Override
    public int hashCode() {
        return Objects.hash(securityId, userName, passHash, salt);
    }
}
