package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "contest_entries")
public class ContestEntry implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_entry_id")
    private int contestEntryId;

    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "user_id")
    private int userId;

    @OneToOne(mappedBy = "contestEntry", cascade = CascadeType.ALL)
    private Entry entry;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_entry_id")
    private Set<Review> reviews;

    public ContestEntry(int contestEntryId, int contestId, int userId, Entry entry) {
        this.contestEntryId = contestEntryId;
        this.contestId = contestId;
        this.userId = userId;
        this.entry = entry;
    }

    public ContestEntry() {
    }

    @Override
    public int getId() {
        return contestEntryId;
    }

    @Override
    public void setId(int contestEntryId) {
        this.contestEntryId = contestEntryId;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContestEntry that = (ContestEntry) o;
        return contestEntryId == that.contestEntryId &&
                contestId == that.contestId &&
                userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(contestEntryId, contestId, userId);
    }
}
