package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "personal_info")
public class PersonalInfo implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "personal_info_id")
    private int personalInfoId;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "user_name")
    private String userName;

    public PersonalInfo(int id, String firstName, String lastName, String userName) {
        this.personalInfoId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
    }

    public PersonalInfo() {
    }

    @Override
    public int getId() {
        return personalInfoId;
    }

    @Override
    public void setId(int id) {
        this.personalInfoId = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonalInfo that = (PersonalInfo) o;
        return personalInfoId == that.personalInfoId &&
                firstName.equals(that.firstName) &&
                lastName.equals(that.lastName) &&
                userName.equals(that.userName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personalInfoId, firstName, lastName, userName);
    }
}
