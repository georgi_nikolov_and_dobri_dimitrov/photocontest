package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.photoContest.utils.Constants.*;

@Entity
@Table(name = "users")
public class User implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int userId;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "personal_Info_id")
    private PersonalInfo personalInfo;

    @Column(name = "rank")
    private int rank;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private Set<ContestEntry> contestEntries;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "contest_invitations",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "contest_id"))
    private Set<Contest> invitational;

    public User() {
    }

    public User(int id, PersonalInfo personalInfo, int rank, Set<ContestEntry> contestEntries) {
        this.userId = id;
        this.personalInfo = personalInfo;
        this.rank = rank;
        this.contestEntries = contestEntries;
    }

    @Override
    public int getId() {
        return userId;
    }

    @Override
    public void setId(int id) {
        this.userId = id;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public Set<ContestEntry> getContestEntries() {
        return contestEntries;
    }

    public void setContestEntries(Set<ContestEntry> contestEntries) {
        this.contestEntries = contestEntries;
    }

    public String getFirstName() {
        return personalInfo.getFirstName();
    }

    public String getLastName() {
        return personalInfo.getLastName();
    }

    public String getUserName() {
        return personalInfo.getUserName();
    }

    public List<Entry> getEntries() {
        return getContestEntries().stream()
                .map(ContestEntry::getEntry)
                .collect(Collectors.toList());
    }

    public String getRankTitle() {
        if (rank <= JUNKIE_RANK_UP_TO)
            return JUNKIE_TITLE;
        else if (rank <= ENTHUSIAST_RANK_UP_TO)
            return ENTHUSIAST_TITLE;
        else if (rank <= MASTER_RANK_UP_TO)
            return MASTER_TITLE;
        else
            return DICTATOR_TITLE;
    }

    public Set<Contest> getInvitational() {
        return invitational;
    }

    public void setInvitational(Set<Contest> invitational) {
        this.invitational = invitational;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return userId == user.userId && rank == user.rank && personalInfo.equals(user.personalInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, personalInfo, rank);
    }
}
