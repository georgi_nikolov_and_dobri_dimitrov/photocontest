package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "contest_invitations")
public class ContestInvitation implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_invitation_id")
    private int contestInvitationId;

    @OneToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @Column(name = "user_id")
    private int userId;

    public ContestInvitation() {
    }

    public ContestInvitation(int contestInvitationId, Contest contest, int userId) {
        this.contestInvitationId = contestInvitationId;
        this.contest = contest;
        this.userId = userId;
    }

    @Override
    public int getId() {
        return contestInvitationId;
    }

    @Override
    public void setId(int contestInvitationId) {
        this.contestInvitationId = contestInvitationId;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContestInvitation that = (ContestInvitation) o;
        return contestInvitationId == that.contestInvitationId && contest.equals(that.contest) && userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(contestInvitationId, contest, userId);
    }
}
