package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Future;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "contests")
public class Contest implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contest_id")
    private int contestId;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "contest_category_id")
    private ContestCategory contestCategory;

    @JsonIgnore
    @Column(name = "is_open")
    private boolean isOpen;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(name = "created")
    private LocalDateTime created;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(name = "phase_1")
    private LocalDateTime phase1;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(name = "phase_2")
    private LocalDateTime phase2;

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
//    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(name = "end_date")
    private LocalDateTime endDate;

//    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_id")
    private Set<ContestEntry> contestEntries;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "contest_id")
    private Set<JuryPerson> jury;

    @JsonIgnore
    @Column(name = "is_scored")
    private boolean isScored;

    public Contest() {
    }

    public Contest(int contestId, String title, ContestCategory contestCategory,
                   boolean isOpen, LocalDateTime created, LocalDateTime phase1,
                   LocalDateTime phase2, LocalDateTime endDate,
                   Set<ContestEntry> contestEntries, Set<JuryPerson> jury, boolean isScored) {
        this.contestId = contestId;
        this.title = title;
        this.contestCategory = contestCategory;
        this.isOpen = isOpen;
        this.created = created;
        this.phase1 = phase1;
        this.phase2 = phase2;
        this.endDate = endDate;
        this.contestEntries = contestEntries;
        this.jury = jury;
        this.isScored = isScored;
    }

    @Override
    public int getId() {
        return contestId;
    }

    @Override
    public void setId(int contestId) {
        this.contestId = contestId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ContestCategory getContestCategory() {
        return contestCategory;
    }

    public void setContestCategory(ContestCategory contestCategory) {
        this.contestCategory = contestCategory;
    }

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getPhase1() {
        return phase1;
    }

    public void setPhase1(LocalDateTime phase1) {
        this.phase1 = phase1;
    }

    public LocalDateTime getPhase2() {
        return phase2;
    }

    public void setPhase2(LocalDateTime phase2) {
        this.phase2 = phase2;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Set<ContestEntry> getContestEntries() {
        return contestEntries;
    }

    public void setContestEntries(Set<ContestEntry> contestEntries) {
        this.contestEntries = contestEntries;
    }

    public Set<String> getJuryUsernames() {
        return jury.stream().map(juryPerson -> juryPerson.getPersonalInfo().getUserName()).collect(Collectors.toSet());
    }

    public Set<JuryPerson> getJury() {
        return jury;
    }

    public void setJury(Set<JuryPerson> jury) {
        this.jury = jury;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean getIsScored() {
        return isScored;
    }

    public void setIsScored(boolean isScored) {
        this.isScored = isScored;
    }

    /*    public List<String> getJuryUserNames() {
        return getJury().stream()
                .map(JuryPerson::getPersonalInfo)
                .map(PersonalInfo::getUserName)
                .collect(Collectors.toList());
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contest contest = (Contest) o;
        return contestId == contest.contestId &&
                isOpen == contest.isOpen &&
                title.equals(contest.title) &&
                contestCategory.equals(contest.contestCategory) &&
                created.equals(contest.created) &&
                phase1.equals(contest.phase1) &&
                phase2.equals(contest.phase2) &&
                endDate.equals(contest.endDate) &&
                Objects.equals(contestEntries, contest.contestEntries);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contestId, title, contestCategory, isOpen, created,
                phase1, phase2, endDate, contestEntries);
    }

    public String getCurrentPhase() {
        String phaseName;
        if (getPhase1().isAfter(LocalDateTime.now())) {
            phaseName = "Pre-open";
        } else if (getPhase1().isBefore(LocalDateTime.now()) && getPhase2().isAfter(LocalDateTime.now())) {
            phaseName = "Phase 1";
        } else if (getPhase2().isBefore(LocalDateTime.now()) && getEndDate().isAfter(LocalDateTime.now())) {
            phaseName = "Phase 2";
        } else {
            phaseName = "Expired";
        }
        return phaseName;
    }
}
