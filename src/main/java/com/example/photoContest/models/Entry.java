package com.example.photoContest.models;

import com.example.photoContest.models.contracts.Identify;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "entries")
public class Entry implements Identify {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "entry_id")
    private int entryId;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "photo_location")
    private String photoLocation;

    @OneToOne
    @JoinColumn(name = "contest_entry_id")
    @JsonIgnore
    private ContestEntry contestEntry;

    public Entry() {
    }

    public Entry(int entryId, ContestEntry contestEntry, String title, String description, String photoLocation) {
        this.entryId = entryId;
        this.contestEntry = contestEntry;
        this.title = title;
        this.description = description;
        this.photoLocation = photoLocation;
    }

    @Override
    public int getId() {
        return entryId;
    }

    @Override
    public void setId(int entryId) {
        this.entryId = entryId;
    }

    public int getContestEntryId() {
        return contestEntry.getId();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoLocation() {
        return photoLocation;
    }

    public void setPhotoLocation(String photoLocation) {
        this.photoLocation = photoLocation;
    }

    public ContestEntry getContestEntry() {
        return contestEntry;
    }

    public void setContestEntry(ContestEntry contestEntry) {
        this.contestEntry = contestEntry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Entry entry = (Entry) o;
        return entryId == entry.entryId && title.equals(entry.title) && description.equals(entry.description) && photoLocation.equals(entry.photoLocation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(entryId, title, description, photoLocation);
    }
}
