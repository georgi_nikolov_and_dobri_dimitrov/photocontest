package com.example.photoContest;

import com.example.photoContest.utils.config.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class PhotoContest {

    public static void main(String[] args) {
        SpringApplication.run(PhotoContest.class, args);
    }

}
