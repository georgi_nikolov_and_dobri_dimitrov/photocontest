package com.example.photoContest.repositories;

import com.example.photoContest.models.Review;
import com.example.photoContest.repositories.contracts.ReviewRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class ReviewRepositoryImpl implements ReviewRepository {
    private static final String MODEL_NAME = "Review";
    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Review create(Review review) {
        try (Session session = sessionFactory.openSession()) {
            session.save(review);
            return getByName(review);
        }
    }

    @Override
    public Review getByName(Review review) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery(
                    " from Review as r " +
                            " where r.author = :author " +
                            " and r.textReview = :textReview " +
                            " and r.contentEntryId = :contentEntryId " +
                            " and r.score = :score ", Review.class);
            query.setParameter("author", review.getAuthor());
            query.setParameter("textReview", review.getTextReview());
            query.setParameter("contentEntryId", review.getContentEntryId());
            query.setParameter("score", review.getScore());

            List<Review> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public Review getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Review entity = session.get(Review.class, id);
            if (entity == null)
                throw new NotFoundException(notFound(MODEL_NAME));
            return entity;
        }
    }

    @Override
    public List<Review> getByContest(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery(
                    " select r " +
                            " from Contest as c " +
                            " join c.jury as j " +
                            " join j.reviews as r " +
                            " where c.contestId = :contestId ", Review.class);
            query.setParameter("contestId", contestId);

            List<Review> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result;
        }
    }


}
