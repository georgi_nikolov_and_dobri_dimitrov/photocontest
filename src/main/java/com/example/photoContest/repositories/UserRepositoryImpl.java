package com.example.photoContest.repositories;

import com.example.photoContest.models.PersonalInfo;
import com.example.photoContest.models.User;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private static final String MODEL_NAME = "User";
    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    " select u " +
                            " from User as u " +
                            " order by u.rank DESC ", User.class);
            List<User> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result;
        }
    }

    @Override
    public User create(User entity) {
        try (Session session = sessionFactory.openSession()) {
            session.save(entity);
            return getByName(entity);
        }
    }

    @Override
    public User getByName(User user) {
        PersonalInfo info = user.getPersonalInfo();
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    " select u " +
                            " from User as u " +
                            " join u.personalInfo as i " +
                            " where " +
                            " u.rank = :rank " +
                            " and i.firstName = :firstName " +
                            " and i.lastName = :lastName " +
                            " and i.userName = :userName ", User.class);
            query.setParameter("rank", user.getRank());
            query.setParameter("firstName", info.getFirstName());
            query.setParameter("lastName", info.getLastName());
            query.setParameter("userName", info.getUserName());

            List<User> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public User getByUserName(String userName) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    " select u " +
                            " from User as u " +
                            " join u.personalInfo as i " +
                            " where i.userName = :userName ", User.class);
            query.setParameter("userName", userName);

            List<User> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            User entity = session.get(User.class, id);
            if (entity == null)
                throw new NotFoundException(notFound(MODEL_NAME));
            return entity;
        }
    }

    @Override
    public User getByContestEntryId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    " select u " +
                            " from User as u " +
                            " join u.contestEntries as ce" +
                            " where ce.contestEntryId = :id ", User.class);
            query.setParameter("id", id);

            List<User> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public User update(User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }

        return user;
    }

    @Override
    public List<String> getAllUserNames() {
        try (Session session = sessionFactory.openSession()) {
            Query<String> query = session.createQuery(
                    " select i.userName " +
                            " from User as u " +
                            " join u.personalInfo as i " +
                            " order by u.rank DESC ", String.class);
            List<String> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result;
        }
    }
}
