package com.example.photoContest.repositories;

import com.example.photoContest.models.Organiser;
import com.example.photoContest.models.PersonalInfo;
import com.example.photoContest.repositories.contracts.OrganiserRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class OrganiserRepositoryImpl implements OrganiserRepository {
    private static final String MODEL_NAME = "Organiser";
    private final SessionFactory sessionFactory;

    @Autowired
    public OrganiserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Organiser getByName(Organiser organiser) {
        PersonalInfo info = organiser.getPersonalInfo();

        try (Session session = sessionFactory.openSession()) {
            Query<Organiser> query = session.createQuery(
                    " select o " +
                            " from Organiser as o " +
                            " join o.personalInfo as i " +
                            " where " +
                            " i.firstName = :firstName " +
                            " and i.lastName = :lastName " +
                            " and i.userName = :userName ", Organiser.class);
            query.setParameter("firstName", info.getFirstName());
            query.setParameter("lastName", info.getLastName());
            query.setParameter("userName", info.getUserName());

            List<Organiser> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public Organiser getByUserName(String userName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Organiser> query = session.createQuery(
                    " select o " +
                            " from Organiser as o " +
                            " join o.personalInfo as i " +
                            " where i.userName = :userName ", Organiser.class);
            query.setParameter("userName", userName);

            List<Organiser> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public Organiser getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Organiser entity = session.get(Organiser.class, id);
            if (entity == null)
                throw new NotFoundException(notFound(MODEL_NAME));
            return entity;
        }
    }

    @Override
    public List<Organiser> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Organiser> query = session.createQuery(
                    " from Organiser as o ", Organiser.class);
            List<Organiser> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result;
        }
    }

    @Override
    public Organiser create(Organiser entity) {
        try (Session session = sessionFactory.openSession()) {
            session.save(entity);
            return getByName(entity);
        }
    }
}
