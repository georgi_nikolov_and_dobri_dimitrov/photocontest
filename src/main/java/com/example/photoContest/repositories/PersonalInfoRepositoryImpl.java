package com.example.photoContest.repositories;

import com.example.photoContest.models.PersonalInfo;
import com.example.photoContest.repositories.contracts.PersonalInfoRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Constants.JURY_MIN_RANK;
import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class PersonalInfoRepositoryImpl implements PersonalInfoRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PersonalInfoRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PersonalInfo create(PersonalInfo personalInfo) {
        try (Session session = sessionFactory.openSession()) {
            session.save(personalInfo);
            return personalInfo;
        }
    }

    @Override
    public boolean isUnique(String userName) {
        try (Session session = sessionFactory.openSession()) {
            Query<String> query = session.createQuery(
                    " select userName " +
                            " from PersonalInfo as i " +
                            " where i.userName = :userName ", String.class);
            query.setParameter("userName", userName);

            List<String> result = query.list();
            return result.size() == 0;
        }
    }

    @Override
    public List<PersonalInfo> getOptionalJury() {
        try (Session session = sessionFactory.openSession()) {
            Query<PersonalInfo> query = session.createQuery(
                    " select pi " +
                            " from User as u " +
                            " join u.personalInfo as pi " +
                            " where u.rank >= :neededRank ", PersonalInfo.class);
            query.setParameter("neededRank", JURY_MIN_RANK);
            List<PersonalInfo> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound("User"));
            return result;
        }
    }

    @Override
    public PersonalInfo getByUserName(String userName) {
        try (Session session = sessionFactory.openSession()) {
            Query<PersonalInfo> query = session.createQuery(
                    " select pi " +
                            " from PersonalInfo as pi " +
                            " where pi.userName = :userName ", PersonalInfo.class);
            query.setParameter("userName", userName);
            List<PersonalInfo> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound("Personal info"));
            return result.get(0);
        }
    }
}
