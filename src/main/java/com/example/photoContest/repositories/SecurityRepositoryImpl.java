package com.example.photoContest.repositories;

import com.example.photoContest.models.Security;
import com.example.photoContest.repositories.contracts.SecurityRepository;
import com.example.photoContest.utils.errors.DuplicateException;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class SecurityRepositoryImpl implements SecurityRepository {
    private static final String MODEL_NAME = "Security";
    private final SessionFactory sessionFactory;

    @Autowired
    public SecurityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Security create(Security security) {
        try (Session session = sessionFactory.openSession()) {
            session.save(security);
            return getByName(security);
        }
    }

    @Override
    public Security getByName(Security security) {
        try (Session session = sessionFactory.openSession()) {
            Query<Security> query = session.createQuery(
                    " from Security as s " +
                            " where " +
                            " s.salt = :salt " +
                            " and s.passHash = :passHash " +
                            " and s.userName = :userName ", Security.class);
            query.setParameter("salt", security.getSalt());
            query.setParameter("passHash", security.getPassHash());
            query.setParameter("userName", security.getUserName());

            List<Security> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public Security getByUserName(String userName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Security> query = session.createQuery(" from Security as s " +
                    " where s.userName = :userName ", Security.class);
            query.setParameter("userName", userName);
            List<Security> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public void validateUnique(String userName) {
        try {
            getByUserName(userName);
            throw new DuplicateException("Username is taken");
        } catch (NotFoundException ignored) {
        }
    }
}
