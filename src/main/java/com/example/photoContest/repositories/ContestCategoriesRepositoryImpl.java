package com.example.photoContest.repositories;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.ContestCategory;
import com.example.photoContest.repositories.contracts.ContestCategoriesRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Repository
public class ContestCategoriesRepositoryImpl implements ContestCategoriesRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ContestCategoriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ContestCategory> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(" from ContestCategory ", ContestCategory.class)
                    .list();
        }
    }

    @Override
    public ContestCategory getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ContestCategory category = session.get(ContestCategory.class, id);
            if (category==null){
                throw new NotFoundException(String.format("Category with ID %s not found!", id));
            }
            return category;
        }
    }

    @Override
    public ContestCategory getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestCategory> query = session.createQuery("from ContestCategory where name like concat('%', :name, '%')", ContestCategory.class);
            query.setParameter("name", name);
            List<ContestCategory> result = query.list();
            if (result.size()==0){
                throw new NotFoundException(String.format("Category with name %s not found!", name));
            }
            return result.get(0);
        }
    }
}
