package com.example.photoContest.repositories;

import com.example.photoContest.models.ContestEntry;
import com.example.photoContest.repositories.contracts.ContestEntryRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class ContestEntryRepositoryImpl implements ContestEntryRepository {

    private static final String MODEL_NAME = "Contest entry";
    private final SessionFactory sessionFactory;

    @Autowired
    public ContestEntryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public ContestEntry create(ContestEntry entry) {
        try (Session session = sessionFactory.openSession()) {
            session.save(entry);
            return getByName(entry);
        }
    }

    @Override
    public List<ContestEntry> getByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestEntry> query = session.createQuery(
                    " select ce " +
                    " from ContestEntry as ce " +
                            "where " +
                            "ce.userId = :userId", ContestEntry.class);
            query.setParameter("userId", id);

            return query.list();
        }
    }

    @Override
    public ContestEntry getByName(ContestEntry entry) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestEntry> query = session.createQuery(
                            " select ce " +
                            " from ContestEntry as ce " +
                            " where " +
                            " ce.contestId = :contestId " +
                            " and ce.userId = :userId ", ContestEntry.class);
            query.setParameter("contestId", entry.getContestId());
            query.setParameter("userId", entry.getUserId());

            List<ContestEntry> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public ContestEntry getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ContestEntry entity = session.get(ContestEntry.class, id);
            if (entity == null)
                throw new NotFoundException(notFound(MODEL_NAME));
            return entity;
        }
    }

    @Override
    public ContestEntry update(ContestEntry contestEntry) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(contestEntry);
            session.getTransaction().commit();
        }

        return getById(contestEntry.getContestId());
    }
}
