package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.Security;

public interface SecurityRepository {
    Security create(Security security);

    Security getByName(Security security);

    Security getByUserName(String userName);

    void validateUnique(String userName);
}
