package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.JuryPerson;
import com.example.photoContest.models.PersonalInfo;

import java.util.List;

public interface JuryRepository {
    JuryPerson create(JuryPerson entity);

    List<JuryPerson> create(List<JuryPerson> entity);

    JuryPerson getByName(JuryPerson jury);

    JuryPerson getByUserName(String userName);

    JuryPerson getById(int id);

    List<JuryPerson> getByContest(int contestId);

    JuryPerson delete(JuryPerson personToRemove);

}
