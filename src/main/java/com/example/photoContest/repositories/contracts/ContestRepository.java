package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.User;

import java.util.List;

public interface ContestRepository {

    List<Contest> getOpenContests();

    List<Contest> getAllContests();

    List<Contest> getContestsForScoring();

    List<Contest> getInPhaseOne();

    List<Contest> getActiveForJury(String userName);

    Contest create(Contest contest);

    Contest getByName(Contest contest);

    Contest getByTitle(String title);

    Contest getById(int id);

    Contest getByContestEntryId(int id);

    List<Contest> getAllContestsForJuryUser(String username);

    List<Contest> getAllContestInvitesForUser(int userId);

    Contest update(Contest contest);
}
