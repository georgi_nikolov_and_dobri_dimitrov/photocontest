package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.Entry;

import java.util.List;

public interface EntryRepository {
    Entry create(Entry entity);

    Entry getByName(Entry entry);

    Entry getByTitle(String title);

    List<Entry> getByContestId(int contestId);

    Entry getById(int id);

    List<Entry> getByUser(int userId);
}
