package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.PersonalInfo;

import java.util.List;

public interface PersonalInfoRepository {

    PersonalInfo getByUserName(String userName);

    PersonalInfo create(PersonalInfo personalInfo);

    boolean isUnique(String userName);

    List<PersonalInfo> getOptionalJury();
}
