package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.Image;

public interface PhotoRepository {

    Image insertRecords(Image image);

    Image getByEntryId(int entryId);
}
