package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.ContestInvitation;

import java.util.List;

public interface ContestInvitationRepository {
    ContestInvitation create(ContestInvitation entity);

    List<ContestInvitation> create(List<ContestInvitation> entities);

    ContestInvitation getByName(ContestInvitation contestInvitation);

    ContestInvitation getById(int id);
}
