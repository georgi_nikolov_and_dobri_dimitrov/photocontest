package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.Organiser;
import com.example.photoContest.models.User;

import java.util.List;

public interface OrganiserRepository {
    Organiser getByName(Organiser user);

    Organiser getByUserName(String userName);

    List<Organiser> getAll();

    Organiser create(Organiser user);

    Organiser getById(int id);
}
