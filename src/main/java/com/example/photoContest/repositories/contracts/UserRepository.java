package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.User;

import java.util.List;

public interface UserRepository {

    List<User> getAll();

    User getByName(User user);

    User getByUserName(String userName);

    User create(User user);

    User getById(int id);

    User getByContestEntryId(int id);

    User update(User user);

    List<String> getAllUserNames();
}
