package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.Review;

import java.util.List;

public interface ReviewRepository {
    Review create(Review review);

    Review getByName(Review review);

    Review getById(int id);

    List<Review> getByContest(int contestId);
}
