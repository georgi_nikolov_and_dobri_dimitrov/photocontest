package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.ContestEntry;

import java.util.List;

public interface ContestEntryRepository {
    ContestEntry create(ContestEntry entry);

    List<ContestEntry> getByUserId(int id);

    ContestEntry getByName(ContestEntry entry);

    ContestEntry getById(int id);

    ContestEntry update(ContestEntry contestEntry);
}
