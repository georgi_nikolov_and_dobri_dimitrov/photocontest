package com.example.photoContest.repositories.contracts;

import com.example.photoContest.models.ContestCategory;

import java.util.List;

public interface ContestCategoriesRepository {

    List<ContestCategory> getAll();

    ContestCategory getById(int id);

    ContestCategory getByName(String name);
}
