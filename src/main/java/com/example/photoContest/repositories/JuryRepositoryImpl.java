package com.example.photoContest.repositories;

import com.example.photoContest.models.JuryPerson;
import com.example.photoContest.repositories.contracts.JuryRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class JuryRepositoryImpl implements JuryRepository {
    private static final String MODEL_NAME = "Jury Person";
    private final SessionFactory sessionFactory;

    @Autowired
    public JuryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public JuryPerson create(JuryPerson entity) {
        try (Session session = sessionFactory.openSession()) {
            session.save(entity);
            return getByName(entity);
        }
    }

    @Override
    public List<JuryPerson> create(List<JuryPerson> entity) {
        try (Session session = sessionFactory.openSession()) {
            entity.forEach(session::save);
            return getByContest(entity.get(0).getContestId());
        }
    }

    @Override
    public JuryPerson getByName(JuryPerson jury) {
        try (Session session = sessionFactory.openSession()) {
            Query<JuryPerson> query = session.createQuery(
                    " select j " +
                            " from JuryPerson as j " +
                            " where " +
                            " j.contestId = :contestId " +
                            " and j.personalInfo = :personalInfo ", JuryPerson.class);
            query.setParameter("contestId", jury.getContestId());
            query.setParameter("personalInfo", jury.getPersonalInfo());

            List<JuryPerson> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public JuryPerson getByUserName(String userName) {
        try (Session session = sessionFactory.openSession()) {
            Query<JuryPerson> query = session.createQuery(
                    " select j " +
                            " from JuryPerson as j " +
                            " join j.personalInfo as i " +
                            " where i.userName = :userName ", JuryPerson.class);
            query.setParameter("userName", userName);

            List<JuryPerson> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public JuryPerson getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            JuryPerson entity = session.get(JuryPerson.class, id);
            if (entity == null)
                throw new NotFoundException(notFound(MODEL_NAME));
            return entity;
        }
    }

    @Override
    public List<JuryPerson> getByContest(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<JuryPerson> query = session.createQuery(
                    " select j" +
                            " from Contest as c " +
                            " join c.jury as j " +
                            " where c.contestId = :contestId ", JuryPerson.class);
            query.setParameter("contestId", contestId);

            List<JuryPerson> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result;
        }
    }

    @Override
    public JuryPerson delete(JuryPerson personToRemove) {
        try (Session session = sessionFactory.openSession()) {
            JuryPerson deleted = getById(personToRemove.getId());
            session.beginTransaction();
            session.delete(personToRemove);
            session.getTransaction().commit();

            return deleted;
        }
    }

}
