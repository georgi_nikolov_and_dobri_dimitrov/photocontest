package com.example.photoContest.repositories;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.ContestInvitation;
import com.example.photoContest.models.User;
import com.example.photoContest.repositories.contracts.ContestInvitationRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class ContestInvitationRepositoryImpl implements ContestInvitationRepository {

    private static final String MODEL_NAME = "Contest invitation";
    private final SessionFactory sessionFactory;

    @Autowired
    public ContestInvitationRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public ContestInvitation create(ContestInvitation entity) {
        try (Session session = sessionFactory.openSession()) {
            session.save(entity);
            return getByName(entity);
        }
    }

    @Override
    public List<ContestInvitation> create(List<ContestInvitation> entities) {
        try (Session session = sessionFactory.openSession()) {
            entities.forEach(session::save);
            return getByContest(entities.get(0).getContest());
        }
    }

    @Override
    public ContestInvitation getByName(ContestInvitation contestInvitation) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestInvitation> query = session.createQuery(
                    " from ContestInvitation as ci " +
                            " where " +
                            " ci.userId = :userId " +
                            " and ci.contest = :contest ", ContestInvitation.class);
            query.setParameter("userId", contestInvitation.getUserId());
            query.setParameter("contest", contestInvitation.getContest());

            List<ContestInvitation> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public ContestInvitation getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ContestInvitation entity = session.get(ContestInvitation.class, id);
            if (entity == null)
                throw new NotFoundException(notFound(MODEL_NAME));
            return entity;
        }
    }

    private List<ContestInvitation> getByContest(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestInvitation> query = session.createQuery(
                    " from ContestInvitation as ci " +
                            " where " +
                            " ci.contest = :contest ", ContestInvitation.class);
            query.setParameter("contest", contest);

            List<ContestInvitation> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result;
        }
    }
}
