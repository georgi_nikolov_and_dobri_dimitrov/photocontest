package com.example.photoContest.repositories;

import com.example.photoContest.models.Entry;
import com.example.photoContest.repositories.contracts.EntryRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class EntryRepositoryImpl implements EntryRepository {
    private static final String MODEL_NAME = "Entry";
    private final SessionFactory sessionFactory;

    @Autowired
    public EntryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override// must be created after ContestEntry
    public Entry create(Entry entry) {
        try (Session session = sessionFactory.openSession()) {
            session.save(entry);
            return getByName(entry);
        }
    }

    @Override
    public Entry getByName(Entry entry) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery(
                    " select e " +
                            " from Entry as e " +
                            " where " +
                            " e.title = :title " +
                            " and e.description = :description " +
                            " and e.photoLocation = :photoLocation ", Entry.class);
            query.setParameter("title", entry.getTitle());
            query.setParameter("description", entry.getDescription());
            query.setParameter("photoLocation", entry.getPhotoLocation());

            List<Entry> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public Entry getByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery(
                    " select e " +
                            " from Entry as e " +
                            " where e.title = :title", Entry.class);
            query.setParameter("title", title);
            List<Entry> result = query.list();
            if (result.size() == 0) {
                throw new NotFoundException(notFound(MODEL_NAME));
            }
            return result.get(0);
        }
    }

    @Override
    public List<Entry> getByContestId(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery(
                    " select e " +
                            " from Entry as e " +
                            " join ContestEntry as ce " +
                            " where e.contestEntry.id = ce.contestEntryId" +
                            " and ce.contestId = :contestId ", Entry.class);
            query.setParameter("contestId", contestId);
            List<Entry> result = query.list();
            if (result.size() == 0) {
                throw new NotFoundException(notFound(MODEL_NAME));
            }
            return result;
        }
    }

    @Override
    public Entry getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Entry entity = session.get(Entry.class, id);
            if (entity == null)
                throw new NotFoundException(notFound(MODEL_NAME));
            return entity;
        }
    }

    @Override
    public List<Entry> getByUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery(
                    " select e " +
                            " from User as u " +
                            " join u.contestEntries as ce " +
                            " join ce.entry as e" +
                            " where " +
                            " u.userId = :userId ", Entry.class);
            query.setParameter("userId", userId);

            List<Entry> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result;
        }
    }

}
