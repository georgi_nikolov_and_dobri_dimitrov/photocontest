package com.example.photoContest.repositories;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.User;
import com.example.photoContest.repositories.contracts.ContestRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class ContestRepositoryImpl implements ContestRepository {

    private static final String MODEL_NAME = "Contest";
    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Contest> getOpenContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(" select c from Contest as c " +
                    " where c.isOpen = :isOpen " +
                    " order by c.phase1 ", Contest.class);
            query.setParameter("isOpen", true);
            return query.list();
        }
    }

    @Override
    public List<Contest> getAllContests() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(" select c from Contest as c " +
                    " order by c.phase1 ", Contest.class);
            return query.list();
        }
    }

    @Override
    public List<Contest> getContestsForScoring() {
        LocalDateTime today = LocalDateTime.now();
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(" select c from Contest as c " +
                    " where c.isScored = false " +
                    " and c.endDate < :today", Contest.class);
            query.setParameter("today", today);
            return query.list();
        }
    }

    @Override
    public List<Contest> getInPhaseOne() {
        LocalDateTime today = LocalDateTime.now();
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    " select c from Contest as c " +
                    " where c.phase1 < :today " +
                    " and c.phase2 > :today", Contest.class);
            query.setParameter("today", today);
            return query.list();
        }
    }

    @Override
    public List<Contest> getActiveForJury(String userName) {
        LocalDateTime today = LocalDateTime.now();
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    " select c " +
                            " from Contest as c " +
                            " join c.jury as j " +
                            " where c.phase1 < :today " +
                            " and c.endDate > :today " +
                            " and j.personalInfo.userName = :userName", Contest.class);
            query.setParameter("today", today);
            query.setParameter("userName", userName);
            return query.list();
        }
    }

    @Override
    public Contest create(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.save(contest);
            return getByName(contest);
        }
    }

    @Override
    public Contest getByName(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    " select c from Contest as c " +
                            " where " +
                            " c.title = :title " +
                            " and c.created = :created " +
                            " and c.phase1 = :phase1 " +
                            " and c.phase2 = :phase2 " +
                            " and c.endDate = :endDate " +
                            " and c.isOpen = :isOpen " +
                            " and c.contestCategory = :category ", Contest.class);
            query.setParameter("title", contest.getTitle());
            query.setParameter("created", contest.getCreated());
            query.setParameter("phase1", contest.getPhase1());
            query.setParameter("phase2", contest.getPhase2());
            query.setParameter("endDate", contest.getEndDate());
            query.setParameter("isOpen", contest.getIsOpen());
            query.setParameter("category", contest.getContestCategory());
            List<Contest> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public Contest getByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    " select c from Contest as c " +
                            " where c.title = :title", Contest.class);
            query.setParameter("title", title);
            List<Contest> result = query.list();
            if (result.size() == 0) {
                throw new NotFoundException(notFound(MODEL_NAME));
            }
            return result.get(0);
        }
    }

    @Override
    public Contest getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Contest entity = session.get(Contest.class, id);
            if (entity == null)
                throw new NotFoundException(notFound(MODEL_NAME));
            return entity;
        }
    }

    @Override
    public Contest getByContestEntryId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    " select c " +
                            " from Contest as c " +
                            " join c.contestEntries as ce" +
                            " where ce.contestEntryId = :id ", Contest.class);
            query.setParameter("id", id);

            List<Contest> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }

    @Override
    public List<Contest> getAllContestsForJuryUser(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    " select c " +
                            " from Contest as c " +
                            " join c.jury as j" +
                            " where j.personalInfo.userName = :username ", Contest.class);
            query.setParameter("username", username);

            return query.list();
        }
    }

    @Override
    public List<Contest> getAllContestInvitesForUser(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(
                    " select c " +
                            " from ContestInvitation as ci " +
                            " join ci.contest as c" +
                            " where ci.userId = :userId ", Contest.class);
            query.setParameter("userId", userId);

            return query.list();
        }
    }

    @Override
    public Contest update(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(contest);
            session.getTransaction().commit();
        }
        return getById(contest.getId());
    }

}
