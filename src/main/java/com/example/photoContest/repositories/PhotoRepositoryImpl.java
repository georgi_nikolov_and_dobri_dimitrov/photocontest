package com.example.photoContest.repositories;

import com.example.photoContest.models.Image;
import com.example.photoContest.models.User;
import com.example.photoContest.repositories.contracts.PhotoRepository;
import com.example.photoContest.utils.errors.NotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.example.photoContest.utils.Helpers.notFound;

@Repository
public class PhotoRepositoryImpl implements PhotoRepository {
    private static final String MODEL_NAME = "Photo";
    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Image insertRecords(Image image) {
        try (Session session = sessionFactory.openSession()) {
            session.save(image);
            return getByEntryId(image.getEntryId());
        }
    }

    @Override
    public Image getByEntryId(int entryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Image> query = session.createQuery(
                            " from Image as i " +
                            " where i.entryId = :entryId ", Image.class);
            query.setParameter("entryId", entryId);

            List<Image> result = query.list();
            if (result.size() == 0)
                throw new NotFoundException(notFound(MODEL_NAME));
            return result.get(0);
        }
    }
}
