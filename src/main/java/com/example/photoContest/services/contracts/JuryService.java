package com.example.photoContest.services.contracts;

import com.example.photoContest.models.JuryPerson;
import com.example.photoContest.models.PersonalInfo;
import com.example.photoContest.models.Review;
import com.example.photoContest.models.Security;

import java.util.List;

public interface JuryService {
    List<String> getOptionalJury(Security security);

    List<JuryPerson> createJury(Security security, List<String> optionalJury, int contestId);

    JuryPerson selfRemoveFromJury(Security security, int contestId);

    Review create(Review review, Security security);
}
