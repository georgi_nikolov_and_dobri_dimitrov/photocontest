package com.example.photoContest.services.contracts;

import com.example.photoContest.models.User;
import com.example.photoContest.models.dto.Login;
import com.example.photoContest.models.PersonalInfo;
import com.example.photoContest.models.Security;
import com.example.photoContest.utils.Roles;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpSession;

public interface SecurityService {
    Security create(Security security);

    void login(Login login);

    void login(HttpHeaders headers);

    void login(Login login, HttpSession session);

    Security getSecurity(HttpHeaders headers);

    Security getSecurity(HttpSession session);

    PersonalInfo validateRole(Security security, Roles role);

    void setSession(User created, HttpSession session);

    void logout(HttpSession session);
}
