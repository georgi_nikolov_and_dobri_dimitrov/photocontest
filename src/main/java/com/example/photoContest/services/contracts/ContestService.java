package com.example.photoContest.services.contracts;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.ContestInvitation;
import com.example.photoContest.models.Organiser;
import com.example.photoContest.models.Security;

import java.util.List;

public interface ContestService {

    List<Contest> getOpenContests();

    List<Contest> getAllContests();

    List<ContestInvitation> sendInvitations(Security security, List<String> userNames, int contestId);

    Contest create(Contest contest, Security security);

    Contest getByTitle(String title);

    Contest getById(int id);

    List<Contest> getActiveForUser(String userName);

    List<Contest> getActiveForJury(String userName);

    List<Contest> getAllContestsForJuryUser(String username);

    List<Contest> getAllContestInvitesForUser(String username);
}
