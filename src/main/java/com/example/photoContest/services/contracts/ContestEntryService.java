package com.example.photoContest.services.contracts;

import com.example.photoContest.models.ContestEntry;
import com.example.photoContest.models.Security;

import java.util.List;

public interface ContestEntryService {
    ContestEntry createPartial(ContestEntry entry, Security security);

    ContestEntry getById(int id);

    List<ContestEntry> getByUserId(int id);

}
