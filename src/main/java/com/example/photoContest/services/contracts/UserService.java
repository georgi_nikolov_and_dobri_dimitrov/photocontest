package com.example.photoContest.services.contracts;

import com.example.photoContest.models.User;

import java.util.List;

public interface UserService {

    List<String> getAllUserNames();

    User getById(int id);

    User getByName(User user);

    User getByUserName(String userName);

    User create(User user);

    int getPlace(User user);
}
