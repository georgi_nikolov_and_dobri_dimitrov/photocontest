package com.example.photoContest.services.contracts;

import com.example.photoContest.models.ContestInvitation;
import com.example.photoContest.models.Security;

import java.util.List;

public interface ContestInvitationService {
    List<ContestInvitation> create(Security security, List<ContestInvitation> invites);
}
