package com.example.photoContest.services.contracts;

import com.example.photoContest.models.Entry;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.dto.EntryDtoOut;

import java.util.List;

public interface EntryService {

    Entry create(Entry entity, Security security);

    Entry getByTitle(String title);

    List<Entry> getByContestId(int contestId);

    Entry getById(int id);

    List<Entry> getByUser(int userId);

    EntryDtoOut getDto(int entryId);
}
