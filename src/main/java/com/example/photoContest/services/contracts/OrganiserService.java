package com.example.photoContest.services.contracts;

import com.example.photoContest.models.Organiser;
import com.example.photoContest.models.Security;

public interface OrganiserService {
    Organiser getById(int id);

    Organiser getByUserName(String userName);

    Organiser create(Security security, Organiser organiser);

}
