package com.example.photoContest.services.contracts;

import com.example.photoContest.models.Entry;
import com.example.photoContest.models.Security;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {
    Entry storeFile(MultipartFile file, Entry entry, Security security);

    String getFileDownloadUri(String fileName);

    Resource loadFileAsResource(String fileName);
}
