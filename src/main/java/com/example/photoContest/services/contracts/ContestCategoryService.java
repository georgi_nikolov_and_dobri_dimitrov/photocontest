package com.example.photoContest.services.contracts;

import com.example.photoContest.models.ContestCategory;

import java.util.List;

public interface ContestCategoryService {

    List<ContestCategory> getAllCategories();

    ContestCategory getById(int id);

    ContestCategory getByName(String name);
}
