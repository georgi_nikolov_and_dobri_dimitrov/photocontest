package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.repositories.contracts.*;
import com.example.photoContest.services.contracts.OrganiserService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.DuplicateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrganiserServiceImpl implements OrganiserService {

    private final PersonalInfoRepository personalInfo;
    private final SecurityService securityService;
    private final OrganiserRepository repository;

    @Autowired
    public OrganiserServiceImpl(OrganiserRepository repository, PersonalInfoRepository personalInfo,
                                SecurityService securityService) {
        this.securityService = securityService;
        this.personalInfo = personalInfo;
        this.repository = repository;
    }

    @Override
    public Organiser getById(int id) {
        return repository.getById(id);
    }

    @Override
    public Organiser getByUserName(String userName) {
        return repository.getByUserName(userName);
    }

    @Override
    public Organiser create(Security security, Organiser organiser) {
        securityService.validateRole(security, Roles.ORGANISER);
        if (isUnique(organiser.getUserName())) {
            return createOrganiser(organiser);
        } else {
            throw new DuplicateException(String.format("Username %s already exists!",
                    organiser.getUserName()));
        }
    }

    private boolean isUnique(String userName) {
        return personalInfo.isUnique(userName);
    }

    private Organiser createOrganiser(Organiser organiser) {
        PersonalInfo pi = personalInfo.create(organiser.getPersonalInfo());
        organiser.setPersonalInfo(pi);
        return repository.create(organiser);
    }
}
