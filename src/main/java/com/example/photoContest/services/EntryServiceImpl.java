package com.example.photoContest.services;

import com.example.photoContest.models.Contest;
import com.example.photoContest.models.Entry;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.User;
import com.example.photoContest.models.dto.EntryDtoOut;
import com.example.photoContest.repositories.contracts.ContestRepository;
import com.example.photoContest.repositories.contracts.EntryRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.services.contracts.EntryService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.NotOnTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.example.photoContest.utils.Helpers.isBetweenPh1AndPh2;

@Service
public class EntryServiceImpl implements EntryService {

    private final EntryRepository repository;
    private final SecurityService securityService;
    private final ContestRepository contests;
    private final UserRepository users;

    @Autowired
    public EntryServiceImpl(EntryRepository repository, SecurityService securityService, ContestRepository contests, UserRepository users) {
        this.securityService = securityService;
        this.repository = repository;
        this.contests = contests;
        this.users = users;
    }


    @Override
    public Entry create(Entry entity, Security security) {
        securityService.validateRole(security, Roles.USER);
        Contest contest = contests.getByContestEntryId(entity.getContestEntryId());
        if (isBetweenPh1AndPh2(contest))
            return repository.create(entity);
        LocalDateTime today = LocalDateTime.now();
        String message = today.isAfter(contest.getPhase2()) ?
                "Submissions are closed!" : "Submissions are not open yet!";
        throw new NotOnTime(message);
    }

    @Override
    public Entry getByTitle(String title) {
        return repository.getByTitle(title);
    }

    @Override
    public List<Entry> getByContestId(int contestId) {
        return repository.getByContestId(contestId);
    }

    @Override
    public Entry getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<Entry> getByUser(int userId) {
        return repository.getByUser(userId);
    }

    @Override
    public EntryDtoOut getDto(int entryId) {
        Entry entry = getById(entryId);
        User user = users.getByContestEntryId(entry.getContestEntryId());
        return new EntryDtoOut(entry, user.getUserName());
    }
}
