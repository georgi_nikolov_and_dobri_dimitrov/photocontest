package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.repositories.contracts.*;
import com.example.photoContest.services.contracts.ContestService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.DuplicateException;
import com.example.photoContest.utils.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
public class ContestServiceImpl implements ContestService {

    private final ContestInvitationRepository invitationRepository;
    private final SecurityService securityService;
    private final OrganiserRepository organisers;
    private final ContestRepository repository;
    private final UserRepository users;
    private final JuryRepository jury;

    @Autowired
    ContestServiceImpl(ContestInvitationRepository invitationRepository, ContestRepository repository,
                       SecurityService securityService, OrganiserRepository organisers,
                       UserRepository users, JuryRepository jury) {
        this.invitationRepository = invitationRepository;
        this.securityService = securityService;
        this.repository = repository;
        this.organisers = organisers;
        this.users = users;
        this.jury = jury;
    }

    @Override
    public List<Contest> getOpenContests() {
        return repository.getOpenContests();
    }

    @Override
    public List<Contest> getAllContests() {
        return repository.getAllContests();
    }

    @Override
    public List<Contest> getActiveForUser(String userName) {
        User user = users.getByUserName(userName);
        List<Contest> active = repository.getInPhaseOne();
        return getFor(user, active);
    }

    @Override
    public List<Contest> getActiveForJury(String userName) {
        JuryPerson jp = jury.getByUserName(userName);
        List<Contest> active = repository.getActiveForJury(userName);
        if (active == null || active.size() == 0)
            return new ArrayList<>();
        return active.stream()
                .filter(c -> hasEntriesWithoutReview(c, jp))
                .collect(Collectors.toList());
    }

    @Override
    public Contest create(Contest contest, Security security) {
        validateOrganiser(security);
        validateUniqueTitle(contest);
        return repository.create(contest);
    }

    private void validateUniqueTitle(Contest contest) {
        try {
            repository.getByTitle(contest.getTitle());
            throw new DuplicateException(
                    format("Contest whit title: %s already exists",
                            contest.getTitle()));
        } catch (NotFoundException ignored) {
        }
    }

    @Override
    public Contest getByTitle(String title) {
        return repository.getByTitle(title);
    }

    @Override
    public Contest getById(int id) {
        return repository.getById(id);
    }

    @Override
    public List<ContestInvitation> sendInvitations(Security security, List<String> userNames, int contestId) {
        securityService.validateRole(security, Roles.ORGANISER);
        List<ContestInvitation> invites = getInvites(contestId, userNames);
        return invitationRepository.create(invites);
    }

    private List<ContestInvitation> getInvites(int contestId, List<String> userNames) {
        Contest contest = repository.getById(contestId);
        return userNames.stream()
                .map(userName -> users.getByUserName(userName).getId())
                .map(id -> new ContestInvitation(0, contest, id))
                .collect(Collectors.toList());

    }

    @Override
    public List<Contest> getAllContestsForJuryUser(String username) {
        return repository.getAllContestsForJuryUser(username);
    }

    @Override
    public List<Contest> getAllContestInvitesForUser(String username) {
        User user = users.getByUserName(username);

        return repository.getAllContestInvitesForUser(user.getId());
    }

    private List<Contest> getFor(User user, List<Contest> contests) {
        return contests.stream()
                .filter(c -> c.isOpen() || hasInvitation(user, c))
                .filter(c -> userHasNotApplied(user, c))
                .collect(Collectors.toList());
    }

    private boolean hasInvitation(User user, Contest contest) {
        return user.getInvitational().contains(contest);
    }

    private boolean userHasNotApplied(User user, Contest contest) {
        Set<ContestEntry> set = contest.getContestEntries();
        return set.size() == 0 ||
                set.stream()
                        .map(ContestEntry::getUserId)
                        .noneMatch(id -> id == user.getId());
    }

    private boolean hasEntriesWithoutReview(Contest contest, JuryPerson jury) {
        Set<ContestEntry> set = contest.getContestEntries();
        if (set == null || set.size() == 0)
            return false;
        return set.size() > countReviewsBy(jury, set);

    }

    private Long countReviewsBy(JuryPerson jury, Set<ContestEntry> contestEntries) {

        return contestEntries.stream()
                .filter(ce -> ce.getReviews() != null && ce.getReviews().size() != 0)
                .flatMap(ce -> ce.getReviews().stream())
                .filter(review -> review.getAuthor() == jury.getId())
                .count();
    }

    private void validateOrganiser(Security security) {
        securityService.validateRole(security, Roles.ORGANISER);
    }

}
