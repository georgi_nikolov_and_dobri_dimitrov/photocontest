package com.example.photoContest.services;

import com.example.photoContest.models.ContestInvitation;
import com.example.photoContest.models.Security;
import com.example.photoContest.repositories.contracts.ContestInvitationRepository;
import com.example.photoContest.services.contracts.ContestInvitationService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContestInvitationServiceImpl implements ContestInvitationService {

    private final ContestInvitationRepository repository;
    private final SecurityService securityService;

    public ContestInvitationServiceImpl(ContestInvitationRepository repository, SecurityService securityService) {
        this.repository = repository;
        this.securityService = securityService;
    }

    @Override
    public List<ContestInvitation> create(Security security, List<ContestInvitation> invites){
        securityService.validateRole(security, Roles.ORGANISER);

        return repository.create(invites);
    }
}
