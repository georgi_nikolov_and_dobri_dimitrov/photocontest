package com.example.photoContest.services;

import com.example.photoContest.models.PersonalInfo;
import com.example.photoContest.models.Security;
import com.example.photoContest.models.User;
import com.example.photoContest.models.dto.Scoring;
import com.example.photoContest.repositories.contracts.PersonalInfoRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.services.contracts.UserService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.DuplicateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;
    private final PersonalInfoRepository personalInfo;
    private final SecurityService securityService;

    @Autowired
    public UserServiceImpl(UserRepository repository, PersonalInfoRepository personalInfo, SecurityService securityService) {
        this.repository = repository;
        this.personalInfo = personalInfo;
        this.securityService = securityService;
    }

    @Override
    public List<String> getAllUserNames() {
        return repository.getAllUserNames();
    }

    @Override
    public User getById(int id) {
        return repository.getById(id);
    }

    @Override
    public User getByName(User user) {
        return repository.getByName(user);
    }

    @Override
    public User getByUserName(String userName) {
        return repository.getByUserName(userName);
    }

    @Override
    public User create(User user) {
        if (isUnique(user.getUserName())) {
            return createUser(user);
        } else {
            throw new DuplicateException(String.format("Username %s already exists!",
                    user.getUserName()));
        }
    }

    @Override
    public int getPlace(User user) {
        List<User> all = repository.getAll();
        List<User> ranked = all.stream()
                .sorted(Comparator.comparingInt(User::getRank).reversed())
                .collect(Collectors.toList());
        return ranked.indexOf(user) + 1;
    }

    private User createUser(User user) {
        PersonalInfo pi = personalInfo.create(user.getPersonalInfo());
        user.setPersonalInfo(pi);
        return repository.create(user);
    }

    private boolean isUnique(String userName) {
        return personalInfo.isUnique(userName);
    }

}
