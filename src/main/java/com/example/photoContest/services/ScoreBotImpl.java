package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.models.dto.Scoring;
import com.example.photoContest.repositories.contracts.ContestRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.services.contracts.ScoreBot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.example.photoContest.utils.Constants.*;

@Service
public class ScoreBotImpl implements Runnable, ScoreBot {

    private final ContestRepository contests;
    private final UserRepository users;

    @Autowired
    public ScoreBotImpl(ContestRepository contests, UserRepository users) {
        this.contests = contests;
        this.users = users;
    }

    @Override
    @Scheduled(fixedRate = 1_800_000)
    public void run() {
        scoreContests();
    }

    @Override
    public void manualScoring() {
        scoreContests();
    }

    private void scoreContests() {
        List<Contest> endedContests = contests.getContestsForScoring();
        if (endedContests.size() == 0)
            return;
        endedContests.forEach(this::scoreContest);
    }

    private void scoreContest(Contest contest) {
        Set<ContestEntry> entrySet = contest.getContestEntries();
        setDefaultReviewsTo(entrySet, contest);

        List<Scoring> placementScores = getScoring(entrySet);
        List<Scoring> sortedScores = sortByScore(placementScores);

        assignParticipation(sortedScores, contest.getIsOpen());
        assignScores(sortedScores);
        contest.setIsScored(true);
        contests.update(contest);
    }

    private void setDefaultReviewsTo(Set<ContestEntry> entrySet, Contest contest) {
        int jurySize = contest.getJury().size();
        for (ContestEntry entry : entrySet) {
            addReviewsIn(entry, jurySize);
        }
    }

    private List<Scoring> getScoring(Set<ContestEntry> entrySet) {
        List<Scoring> placementScores = new LinkedList<>();
        entrySet.forEach(contestEntry -> placementScores
                .add(new Scoring(contestEntry.getId(),
                        calculateScore(contestEntry.getReviews()))));
        return placementScores;
    }

    private List<Scoring> sortByScore(List<Scoring> placementScores) {
        return placementScores.stream()
                .sorted(Comparator.comparingDouble(Scoring::getScore).reversed())
                .collect(Collectors.toList());
    }

    private void assignScores(List<Scoring> sortedScores) {
        assignFirstPlace(sortedScores);
        assignSecondPlace(sortedScores);
        assignThirdPlace(sortedScores);
    }

    private void assignParticipation(List<Scoring> sortedScores, boolean isOpen) {
        for (Scoring score : sortedScores) {
            if (isOpen) {
                assignRankToUser(JOINING_OPEN_CONTEST, score.getContestEntryId());
            } else {
                assignRankToUser(INVITED_IN_CONTEST, score.getContestEntryId());
            }
        }
    }

    private void assignThirdPlace(List<Scoring> sortedScores) {
        assignPlace(sortedScores, THIRD_PLACE, SHARED_THIRD_PLACE);
    }

    private void assignSecondPlace(List<Scoring> sortedScores) {
        assignPlace(sortedScores, SECOND_PLACE, SHARED_SECOND_PLACE);
    }

    private void assignPlace(List<Scoring> sortedScores, int soloPrice, int sharedPrice) {
        if (sortedScores.size() == 0)
            return;
        List<Scoring> secondPlace = getTopScored(sortedScores);
        sortedScores.removeAll(secondPlace);

        if (secondPlace.size() == 1) {
            int contentEntryId = secondPlace.get(0).getContestEntryId();
            assignRankToUser(soloPrice, contentEntryId);
        } else {
            assignSharedPlace(secondPlace, sharedPrice);
        }
    }

    private void assignFirstPlace(List<Scoring> sortedScores) {
        List<Scoring> firstPlace = getTopScored(sortedScores);
        sortedScores.removeAll(firstPlace);

        if (firstPlace.size() == 1) {
            int contestEntryId = firstPlace.get(0).getContestEntryId();
            if (sortedScores.size() == 0 ||
                    hasDoublePointsOfSecond(firstPlace.get(0), sortedScores.get(0))) {
                assignRankToUser(DOMINATING_FIRST_PLACE, contestEntryId);
            } else {
                assignRankToUser(FIRST_PLACE, contestEntryId);
            }
        } else {
            assignSharedPlace(firstPlace, SHARED_FIRST_PLACE);
        }
    }

    private boolean hasDoublePointsOfSecond(Scoring firstPlace, Scoring secondPlace) {
        return firstPlace.getScore() >= secondPlace.getScore() * 2;
    }

    private void assignSharedPlace(List<Scoring> sameScoring, int rankForSharedPlace) {
        for (Scoring score : sameScoring) {
            int contestEntryId = score.getContestEntryId();
            assignRankToUser(rankForSharedPlace, contestEntryId);
        }
    }

    private void assignRankToUser(int rank, int contestEntryId) {
        User user = users.getByContestEntryId(contestEntryId);
        user.setRank(user.getRank() + rank);
        users.update(user);
    }

    private List<Scoring> getTopScored(List<Scoring> sortedScores) {
        List<Scoring> topScored = new LinkedList<>();
        int counter = 0;
        do {
            Scoring current = sortedScores.get(counter);
            topScored.add(current);
            counter++;
        } while (hasNext(sortedScores, counter) &&
                nextHasSameScore(sortedScores, counter));
        return topScored;
    }

    private boolean nextHasSameScore(List<Scoring> sortedScores, int counter) {
        return Double.compare(sortedScores.get(0).getScore(),
                sortedScores.get(counter).getScore()) == 0;
    }

    private boolean hasNext(List<Scoring> sortedScores, int counter) {
        return sortedScores.size() > counter;
    }

    private double calculateScore(Set<Review> reviews) {
        double total = 0;
        for (Review review : reviews) {
            total += review.getScore();
        }
        return total / reviews.size();
    }

    /**
     * Accepts entry and jury size, if the jury size is bigger than the reviews of the entry
     *
     * @param entry    specified contest entry
     * @param jurySize the jury size of the particular contest
     */
    private void addReviewsIn(ContestEntry entry, int jurySize) {
        Set<Review> reviews = getReviews(entry);
        int numberOfDefaultReview = jurySize - reviews.size();
        if (numberOfDefaultReview > 0)
            for (int i = 0; i < numberOfDefaultReview; i++) {
                Review review = new Review(0, entry.getId(),
                        -i, " default review " + i, DEFAULT_REVIEW_SCORE);
                reviews.add(review);
            }
    }

    private Set<Review> getReviews(ContestEntry entry) {
        Set<Review> reviews = entry.getReviews();
        return reviews == null ? new HashSet<>() : reviews;
    }

}
