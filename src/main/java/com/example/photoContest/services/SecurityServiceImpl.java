package com.example.photoContest.services;

import com.example.photoContest.models.User;
import com.example.photoContest.models.dto.Login;
import com.example.photoContest.models.PersonalInfo;
import com.example.photoContest.models.Security;
import com.example.photoContest.repositories.contracts.OrganiserRepository;
import com.example.photoContest.repositories.contracts.SecurityRepository;
import com.example.photoContest.repositories.contracts.UserRepository;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

import java.util.Locale;

import static com.example.photoContest.utils.Constants.*;

@Service
public class SecurityServiceImpl implements SecurityService {

    private final OrganiserRepository organisers;
    private final SecurityRepository repository;
    private final UserRepository users;

    @Autowired
    public SecurityServiceImpl(OrganiserRepository organisers, SecurityRepository repository,
                               UserRepository users) {
        this.organisers = organisers;
        this.repository = repository;
        this.users = users;
    }

    @Override
    public Security create(Security security) {
        repository.validateUnique(security.getUserName());
        setSecurityDetails(security);
        return repository.create(security);
    }

    @Override
    public void login(Login login) {
        doLogin(login);
    }

    @Override
    public void login(HttpHeaders headers) {
        validate(headers);
        Login login = new Login(headers.getFirst(HEADER_USER_NAME),
                headers.getFirst(HEADER_PASSWORD));
        doLogin(login);
    }

    @Override
    public void login(Login login, HttpSession session) {
        doLogin(login);
        setSession(login.getUserName(), session);
    }

    @Override
    public void logout(HttpSession session) {
        session.removeAttribute(SESSION_NAME);
        session.removeAttribute(SESSION_ROLE);
    }

    @Override
    public Security getSecurity(HttpHeaders headers) {
        validate(headers);

        try {
            Security security = repository.getByUserName(headers.getFirst(HEADER_USER_NAME));
            validatePass(headers.getFirst(HEADER_PASSWORD), security);
            return security;
        } catch (NotFoundException e) {
            throw new AccessDenied();
        }
    }

    @Override
    public Security getSecurity(HttpSession session) {
        String userName = (String) session.getAttribute(SESSION_NAME);
        if (userName == null) {
            throw new AccessDenied();
        }
        try {
            return repository.getByUserName(userName);
        } catch (NotFoundException e) {
            throw new AccessDenied();
        }
    }

    @Override
    public PersonalInfo validateRole(Security security, Roles role) {
        try {
            switch (role) {
                case USER:
                    return users.getByUserName(security.getUserName())
                            .getPersonalInfo();
                case ORGANISER:
                    return organisers.getByUserName(security.getUserName())
                            .getPersonalInfo();
            }
        } catch (RuntimeException ignored) {}
        throw new AccessDenied();
    }

    @Override
    public void setSession(User user, HttpSession session) {
        setSession(user.getUserName(), session);
    }

    private void setSession(String username, HttpSession session) {
        session.setAttribute(SESSION_NAME, username);
        session.setAttribute(SESSION_ROLE, getRole(username));
    }

    private String getRole(String username) {
        try {
            users.getByUserName(username);
            return Roles.USER.toString().toLowerCase();
        } catch (NotFoundException e) {
            return Roles.ORGANISER.toString().toLowerCase();
        }
    }

    private void doLogin(Login login) {
        try {
            Security security = repository.getByUserName(login.getUserName());
            validatePass(login.getPassWord(), security);
        } catch (NotFoundException e) {
            throw new AccessDenied();
        }
    }

    private void validatePass(String passWord, Security security) {
        String newPassHash = getPassHash(passWord, security.getSalt());
        if (!newPassHash.equals(security.getPassHash()))
            throw new AccessDenied();
    }

    private void setSecurityDetails(Security security) {
        setSalt(security);
        setPassHash(security, security.getSalt());
    }

    private void setSalt(Security security) {
        double salt = Math.random();
        security.setSalt(salt);
    }

    private void setPassHash(Security security, double salt) {
        String passHash = getPassHash(security.getPassHash(), salt);
        security.setPassHash(passHash);
    }

    private String getPassHash(String passWord, double salt) {
        return "" + (passWord + salt).hashCode();
    }

    private void validate(HttpHeaders headers) {
        if (headers.isEmpty() ||
                !headers.containsKey(HEADER_USER_NAME) ||
                !headers.containsKey(HEADER_PASSWORD)) {
            throw new AccessDenied();
        }
    }

}
