package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.repositories.contracts.*;
import com.example.photoContest.services.contracts.JuryService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.NotFoundException;
import com.example.photoContest.utils.errors.NotOnTime;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.example.photoContest.utils.Helpers.*;
import static com.example.photoContest.utils.Mapper.mapJuryPerson;

@Service
public class JuryServiceImpl implements JuryService {

    private final ContestEntryRepository contestEntries;
    private final PersonalInfoRepository personalInfo;
    private final SecurityService securityService;
    private final OrganiserRepository organisers;
    private final ContestRepository contests;
    private final JuryRepository repository;
    private final ReviewRepository reviews;

    public JuryServiceImpl(ContestEntryRepository contestEntries, ReviewRepository reviews, JuryRepository repository,
                           PersonalInfoRepository personalInfo, SecurityService securityService,
                           OrganiserRepository organisers, ContestRepository contests) {
        this.contestEntries = contestEntries;
        this.reviews = reviews;
        this.securityService = securityService;
        this.personalInfo = personalInfo;
        this.organisers = organisers;
        this.repository = repository;
        this.contests = contests;
    }

    @Override
    public List<String> getOptionalJury(Security security) {
        validateOrganiser(security);
        return personalInfo.getOptionalJury().stream()
                .map(PersonalInfo::getUserName).collect(Collectors.toList());
    }

    @Override
    public List<JuryPerson> createJury(Security security, List<String> optionalJury, int contestId) {
        validateOrganiser(security);
        List<JuryPerson> fullJury = getOrganisersJury(contestId);
        if (isValid(optionalJury))
            fullJury.addAll(getOptionalJury(optionalJury, contestId));
        return repository.create(fullJury);
    }

    @Override
    public JuryPerson selfRemoveFromJury(Security security, int contestId) {
        PersonalInfo personalInfo = securityService.validateRole(security, Roles.USER);
        Contest contest = (contests.getById(contestId));

        JuryPerson personToRemove = mapJuryPerson(personalInfo, contestId);
        if (!isAfterPh2(contest))
            return repository.delete(personToRemove);
        throw new NotOnTime("You can't leave Jury in phase 2 of the contest");
    }

    @Override
    public Review create(Review review, Security security) {
        Contest contest = getContest(review);
        JuryPerson juryPerson = getValidJuryPerson(contest, security);
        review.setAuthor(juryPerson.getId());
        if (isBetweenPh2AndEnd(contest))
            return reviews.create(review);

        LocalDateTime today = LocalDateTime.now();
        String message = today.isBefore(contest.getPhase2()) ?
                "You can't submit review until phase 2 starts" :
                "You can't submit review in ended contest";
        throw new NotOnTime(message);
    }

    private JuryPerson getValidJuryPerson(Contest contest, Security security) {
        try {
            JuryPerson juryPerson = repository.getByUserName(security.getUserName());
            Set<JuryPerson> jury = contest.getJury();
            if (!jury.contains(juryPerson))
                throw new AccessDenied();

            return juryPerson;
        } catch (NotFoundException e) {
            throw new AccessDenied();
        }
    }

    private Contest getContest(Review review) {
        try {
            ContestEntry contestEntry = contestEntries.getById(review.getContentEntryId());
            return contests.getById(contestEntry.getContestId());
        } catch (NotFoundException e) {
            throw new AccessDenied();
        }
    }

    private List<JuryPerson> getOrganisersJury(int contestId) {
        return organisers.getAll().stream()
                .map(o -> mapJuryPerson(o.getPersonalInfo(), contestId))
                .collect(Collectors.toList());

    }

    private List<JuryPerson> getOptionalJury(List<String> optionalJury, int contestId) {
        return optionalJury.stream()
                .map(personalInfo::getByUserName)
                .map(info -> mapJuryPerson(info, contestId))
                .collect(Collectors.toList());
    }

    private void validateOrganiser(Security security) {
        securityService.validateRole(security, Roles.ORGANISER);
    }
}
