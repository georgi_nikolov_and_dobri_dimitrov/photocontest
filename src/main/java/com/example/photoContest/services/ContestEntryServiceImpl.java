package com.example.photoContest.services;

import com.example.photoContest.models.*;
import com.example.photoContest.repositories.contracts.*;
import com.example.photoContest.services.contracts.ContestEntryService;
import com.example.photoContest.services.contracts.SecurityService;
import com.example.photoContest.utils.Roles;
import com.example.photoContest.utils.errors.AccessDenied;
import com.example.photoContest.utils.errors.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class ContestEntryServiceImpl implements ContestEntryService {
    private final ContestEntryRepository contestEntryRepository;
    private final SecurityService securityService;
    private final ContestRepository contests;
    private final JuryRepository jury;
    private final UserRepository users;

    @Autowired
    public ContestEntryServiceImpl(ContestEntryRepository contestEntryRepository, SecurityService securityService,
                                   JuryRepository jury, ContestRepository contests, UserRepository users) {
        this.contestEntryRepository = contestEntryRepository;
        this.securityService = securityService;
        this.contests = contests;
        this.users = users;
        this.jury = jury;
    }

    @Override
    public ContestEntry createPartial(ContestEntry contestEntry, Security security) {
        int contestId = contestEntry.getContestId();
        validateEligibleForContest(security, contestId);
        User user = users.getByUserName(securityService.validateRole(security, Roles.USER).getUserName());
        contestEntry.setUserId(user.getId());
        return contestEntryRepository.create(contestEntry);
    }

    @Override
    public ContestEntry getById(int id) {
        return contestEntryRepository.getById(id);
    }

    @Override
    public List<ContestEntry> getByUserId(int id) {
        return contestEntryRepository.getByUserId(id);
    }

    private void validateEligibleForContest(Security security, int contestId) {
        User user = users.getByUserName(security.getUserName());
        Contest contest = contests.getById(contestId);

        validateSingleEntry(user, contestId);
        validateNotJury(security, contestId);
        validateInvitation(user, contest);
    }

    private void validateNotJury(Security security, int contestId) {
        try {
            JuryPerson person = jury.getByUserName(security.getUserName());
            List<JuryPerson> fullJury = jury.getByContest(contestId);
            if (fullJury.contains(person))
                throw new AccessDenied();
        } catch (NotFoundException ignored) {
        }
    }

    private void validateSingleEntry(User user, int contestId) {
        Set<ContestEntry> contestEntries = user.getContestEntries();
        if (contestEntries.size() > 0 &&
                hasDuplicate(contestEntries, contestId))
            throw new AccessDenied();

    }

    private boolean hasDuplicate(Set<ContestEntry> contestEntries, int contestId) {
        return contestEntries.stream()
                .map(ContestEntry::getContestId)
                .anyMatch(id -> id == contestId);
    }

    private void validateInvitation(User user, Contest contest) {
        if (contest.getIsOpen() ||
                user.getInvitational().contains(contest))
            return;
        throw new AccessDenied();
    }

}
