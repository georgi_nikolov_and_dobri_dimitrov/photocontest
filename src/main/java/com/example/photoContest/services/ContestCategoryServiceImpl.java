package com.example.photoContest.services;

import com.example.photoContest.models.ContestCategory;
import com.example.photoContest.repositories.contracts.ContestCategoriesRepository;
import com.example.photoContest.services.contracts.ContestCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContestCategoryServiceImpl implements ContestCategoryService {

    private final ContestCategoriesRepository repository;

    public ContestCategoryServiceImpl(ContestCategoriesRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ContestCategory> getAllCategories() {
        return repository.getAll();
    }

    @Override
    public ContestCategory getById(int id) {
        return repository.getById(id);
    }

    @Override
    public ContestCategory getByName(String name) {
        return repository.getByName(name);
    }
}
